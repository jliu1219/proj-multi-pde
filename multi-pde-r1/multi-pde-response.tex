
\documentclass[11]{article}
\usepackage{spverbatim}
\usepackage[margin=1in]{geometry}
\usepackage{listings,amsmath,amsfonts}
\usepackage{xcolor}
\newcommand{\note}[1]{\textup{{\color{red}#1}}}
\newcommand{\C}{\mathbb{C}}

\DeclareMathOperator{\poly}{poly}

\definecolor{light-gray}{gray}{0.95}

\lstset{basicstyle=\color{blue}\ttfamily,
breaklines=true,
columns=fullflexible,
breakindent=0em,
aboveskip=3ex,
belowskip=1ex,
backgroundcolor=\color{light-gray},
keepspaces=true}
\setlength{\parindent}{0cm}
\setlength{\parskip}{4pt}

\newcommand{\edit}[1]{{\color{green!50!black}{#1}}}
\newcommand{\add}[1]{{\color{cyan}{#1}}}
\usepackage{soul}
\usepackage{bm}

\newcommand{\amc}[1]{{\color{red}{\textbf{#1}}}}
\newcommand{\jpl}[1]{{\color{blue}\textbf{[Jin-Peng: #1]}}}

\begin{document}


% The {lstlisting} environment is like {verbatim} but more customizable (and has word wrapping)
% \end{lstlisting} Response to referee \begin{lstlisting}

Dear Editor:

\medskip

We thank the referees for their assessment of the paper and their extensive and detailed comments. We apologize for the delayed reply. We somehow failed to receive the initial email notification of the reviews and only found out that the first review phase had been completed much later when checking the journal's submission system. It has also taken additional time to thoroughly revise the manuscript.

We believe we have now addressed all the issues that were raised, as outlined below. To more easily identify changes in the manuscript, additions are shown in \add{cyan} and removed text is indicated by \st{strikethrough}.

We also found a calculation error in Theorem 1 that increases the power of $d$ in the complexity of the adaptive FDM approach. These corrections are indicated in \edit{green} to distinguish them from changes made in response to the referees' comments.

Thank you for considering this revised version of the paper, which we hope is now suitable for publication in \emph{Quantum}.

\medskip

Yours sincerely, \\
Andrew Childs, Jin-Peng Liu, and Aaron Ostrander

\bigskip

\begin{lstlisting}
Reviewer #1:

The paper develops adaptive-order finite difference methods and spectral methods for solving partial differential equations (PDEs) on a quantum computer using Harrow-Hassidim-Lloyd-type quantum linear system algorithms with the goal of achieving polylogarithmic dependence on target error. The paper succeeds at this task, but at the cost of a complicated dependence on derivatives of the true solution of the PDE.

The paper is a valuable contribution to the quantum PDE solving literature. It develops a good idea for reducing the error-dependence from $1/\epsilon$ to $log(1/\epsilon)$. Unfortunately, the paper is not clear enough about the complicated relationship of the algorithmic complexity with high order derivatives of the PDE solution. Without that clarity, I cannot evaluate the significance of the contribution and therefore cannot recommend acceptance.

I have worked through most of the Lemmas and each of the main Theorems and satisfied myself of their technical correctness. I was a bit sloppy about Lemmas 10 and 11, and I did not go through the Appendix in much detail.

The technical details of the paper are fairly clear and the first ten pages or so are well structured and written. However, the technical content past about page 10 becomes increasingly challenging to follow, as there is little explanation or interpretation of the technical results.

The main problem, however, is the discussion around Theorems 1 and 2. These theorems state that the complexity of their algorithms depend strongly on high order derivatives of the true solution, but there is little-to-no explanation about how this derivative dependence influences the utility of the algorithm or whether there is a corresponding dependence in classical literature.

I cannot meaningfully evaluate the merit of this work without a clear discussion of the derivative dependence of the algorithms. I also do not think the authors have clearly explained what is "adaptive" about their techniques.

The paper is technically sound but the authors are not being up front about how their algorithms depend on derivatives of the true solution of the PDE. I recommend the following revisions.
\end{lstlisting}

We thank the referee for their assessment of the paper and suggestions for improving the presentation. We agree in general that some aspects of the results could be explained more clearly. We have tried to do this with changes in response to the points listed below.

\begin{lstlisting}
1. Include an informal summary of Theorems 1 and 2 in the introduction.
\end{lstlisting}

We added informal summaries of Theorem 1 and 2 on page 3.

\begin{lstlisting}
2. Clearly explain how the algorithms of Theorems 1 and 2 depend on the derivatives of the true solution of the PDE.
\end{lstlisting}

We explained the dependence of the derivatives of the true solution in both Section 1 and Section 5.

\begin{lstlisting}
3. Move the statement of Theorem 1 to the beginning of Section 3 and include a brief sketch of the proof, explaining where each of Lemmas 1-4 are used.
\end{lstlisting}

We moved Theorem 1 as suggested (see page 6) and described how Lemmas 1--4 are used to establish the proof.

\begin{lstlisting}
4. Move the statement of Theorem 2 to the beginning of Section 4 and include a brief sketch of the proof, explaining where each of Lemmas 5-11 are used.
\end{lstlisting}

Similarly, we moved Theorem 2 as suggested (see pages 13--14) and described how Lemmas 5--11 are used to establish the proof.

\begin{lstlisting}
5. Move Sections 4.1 and 4.4 to the Appendix.
\end{lstlisting}

We appreciate this suggestion. While we think it would be reasonable to include these sections in appendices, we feel that the flow of the paper works better with them as part of the main body. Of course, the reader who feels these results are straightforward can easily skip over them.

Note that these sections do contain some novel technical material.
In Section 4.1, we define the multi-dimensional quantum shifted Fourier transform and quantum cosine transform and describe how to implement them (Lemmas 5 and 6).
% The quantum shifted Fourier transform is first introduced as a kind of quantum transform,
For the quantum cosine transform, we improve the complexity from $O(\log^2 n)$ (in the best previous reference we are aware of) to $O(\log n \log\log n)$ in Lemma 6. Despite being fairly straightforward using well-known ideas, we think this is worth highlighting to address a minor gap in the literature.

In Section 4.4, we describe a procedure for preparing the right-hand side of the linear system. This is a key step in the algorithm, and its complexity is a significant aspect of the overall algorithm's complexity, so we think it belongs as part of the main discussion of the algorithm.

\begin{lstlisting}
6. Completely revise Section 5 to include a thorough discussion of the derivative dependence of Theorems 1 and 2 as well as any correspondence to such derivative dependence in classical methods.
\end{lstlisting}

We revised Section 5 to explain of the dependence of the complexity on the derivatives of the true solution, both for Theorems 1 and 2 and for classical methods. We now explain that classical algorithms also have the same logarithmic dependence on high-order derivatives, and that such a dependence does not have significant impact even for highly oscillatory solutions.

\begin{lstlisting}
7. Clearly explain what is "adaptive" about the authors' techniques.
\end{lstlisting}

The algorithm is adaptive in the sense that the truncation order is chosen in a way that depends on the allowed error. We added an explanation of this on page 3, and between (3.37) and (3.38) on page 11.

\clearpage

\begin{lstlisting}
Reviewer #2:

The manuscript extends quantum algorithms for ordinary differential equations with $\polylog(1/\epsilon)$ scaling to partial differential equations, where $\epsilon$ is the desired precision. The authors present algorithms using the finite difference method and the linear combinations of unitary operators-based quantum linear systems algorithm, as well as using spectral methods. This leads to asymptotic improvements for a variety of quantum algorithms for partial differential equations.

The paper is very clear and well-written, and presents important contributions for quantum algorithms, bringing recent work on quantum algorithms for Hamiltonian simulation / linear systems with $polylog(1/\epsilon)$ to bear on PDEs. The main results improve upon the complexity of a variety of quantum algorithms for PDEs, giving optimal dependence on the precision epsilon. Important concepts are clearly introduced with illustrative examples. (I particularly appreciated the extended example in Appendix A.) I recommend the paper for acceptance in Quantum with some minor edits and additions.
\end{lstlisting}

We thank the referee for their positive assessment of the paper and valuable comments.

\begin{lstlisting}
The paper is generally very well written and clear, with helpful examples, and the literature review is adequate. I think the one area where it could be improved a bit is the addition of more context, both internal and externally. I'll discuss this more in the comments section, but internally, I'd hope e.g. for earlier mention of the importance of global strict diagonal dominance and where it'll factor into the complexities; externally I think some additional information on the place of this work going forward could be added. (This paper integrates techniques from a range of sources to give its advances - where might the developments from this paper be used, beyond the general domain of quantum algorithms for PDEs?)
\end{lstlisting}

Thank you for these suggestions. We agree that the global strict diagonal dominance condition should be mentioned more prominently and that its influence on the complexity could be described more clearly. We now mention this condition in the introduction when first describing the class of problems that we consider. We also explain there that the condition is automatically satisfied for the Poisson equation, and we explain the condition's role in the complexity of our spectral method algorithm. Furthermore, we added some additional reminders about this condition where it is used in Lemma 10.

We added a pointer at the end of the introduction to the discussion of possible future applications of our work in Section 5. We focus on applications within the domain of quantum algorithms, including generalizations of our approach to other boundary conditions and types of PDEs, use as a subroutine in other quantum algorithms (where we slightly expanded the discussion to mention the possibility of applications to optimization), and concrete applications to determining properties of solutions to particular PDEs that model systems of interest. We think there are many interesting outstanding questions about such applications. While it is certainly possible that ideas from our work could also be useful in a broader context (for example, in classical numerical analysis), we do not have specific applications in mind, and we prefer not to claim a broader scope than we feel we can justify.

\begin{lstlisting}
I really appreciate Appendix A; it's only mentioned in one place at the end of Section 4.2, but I might consider adding another reference to it that's easier to find.
\end{lstlisting}

We added a reference to Appendix A right after Theorem 2 at the beginning of Section 4.

\begin{lstlisting}
I think some of the ways the references are applied could be more self-contained. E.g. in (4.107), Eq. (1.8.28) of a book (Ref. [27]) is used, but I don't have access to the book, so it's hard for me to use this.
\end{lstlisting}

We address this point where it is mentioned again below.

\begin{lstlisting}
* I'm concerned that blue text isn't the best approach for Table 1 - what about bolding instead of a color change? A color change could get lost (e.g. in printing) or be harder to see for some readers.
\end{lstlisting}

We changed the blue text to bold text in Table 1 as suggested.

\begin{lstlisting}
* The article would benefit from more discussion of the importance of global strict diagonal dominance. It doesn't become important again until much later, in Lemma 10, Theorem 2, etc. Could more be said about it right after its definition, e.g. why it'll matter etc? I also wasn't sure why this is stronger than strict diagonal dominance: is this because A is a function and (2.8) holds for all x, or are there other reasons? (2.8) should also be explicit that it holds for all x.
\end{lstlisting}

We now explain the significance of global strict diagonal dominance earlier, as described above. Although this condition is used explicitly in the proof of Theorem 2, it also holds for Poisson's equation (as mentioned after (2.8) and now also emphasized in the introduction), so it is an implicit condition of Theorem 1. Thus we state this assumption as our general setting for both of the main results, Theorems 1 and 2.

When introducing this condition, we now mention explicitly that it should hold for all $\bm{x}$. We also added some text and an equation after (2.8) to explain why it is stronger than strict diagonal dominance.

\begin{lstlisting}
* Consistency between "multi-dimensional" and "multidimensional" - both are used currently.
\end{lstlisting}

We changed ``multidimensional" $\to$ ``multi-dimensional" on pages 10, 18, and 19.

\begin{lstlisting}
* Inconsistent use of references in text: occasionally "Ref. [X]" is used, and occasionally "[X]" is used, e.g. page 15 has several examples of this. Also, "reference [X]" appears once on page 26.
\end{lstlisting}

We changed ``Ref. [X]" $\to$ ``reference [X]" on pages 2, 3, 6, 7, 10, and 12; and changed ``[X]" $\to$ ``reference [X]" on pages 3, 16, 17, 19, 28, 29, 33, and 35.

\begin{lstlisting}
* More generally, some of the ways the references are used could be more self-contained. E.g. in (4.107), Eq. (1.8.28) of a book (Ref. [27]) is used, but I don't have access to the book, so it's hard for me to apply this. Also, the variable t here used in the \max doesn't appear in this equation.
\end{lstlisting}

We added a new reference with an available link and fixed the typo ``$t\in[0,T]$" $\to$ ``$\bm{x}$" in (4.107). We still refer to some books in the bibliography that may not be readily accessible online; unfortunately we are not aware of more accessible references in all cases.

\begin{lstlisting}
* I think the backreference to the diagonal dominance conditions (2.8) just before (4.79) could be made more helpful - in general I think it's ideal if the reader doesn't have to follow the backreference. Here, the authors could include the name (as was done for the ellipticity conditions just before), make the equations more similar (moving C to the beginning, and making it clear it's the same variable as used earlier in the paper). I think it's also a little confusing to use "then" here - it makes it sound like (4.79) follows (4.78), when really the only difference was dropping the norm because A_{jj}>0. Maybe "and the global strict diagonal dominance conditions (2.8) simplify to"?
\end{lstlisting}

We replaced ``then (2.8) can be rewritten as" by ``Also, the global strict diagonal dominance condition (2.8) simplifies to" before (4.82) (which was (4.79) in the previous version).

\begin{lstlisting}
* "AM-GM inequality" - this is the only use of the inequality; it would be better to spell out the full name
\end{lstlisting}

This name is fairly standard, but for additional clarity, we now also refer to it as the ``inequality of arithmetic and geometric means''.

\begin{lstlisting}
* I was a little confused by the parameter q in Lemma 11. Is it possible for terms in the denominator to cancel, and is the original problem trivial in this case? I have the same question for 4.101.
\end{lstlisting}

% Thanks for the question. We cannot rule out the possibility of cancelations in the denominator of the quantity $q$ in Lemma 11 (and correspondingly, in the state of (4.101), which is now (4.104) in the revised version). If this quantity is very small, then the complexity of our state preparation procedure is very large. We do not see a reason why the problem would be trivial in that case, or otherwise special. However, we think such cancelations are unlikely so they will not present an obstacle in practice.

% \amc{I guess this results from simply adding together the two systems of equations (one for the differential equations and one for the boundary conditions). Maybe we could point out that if there is an accidental cancelation, we could multiply one of the two systems of equations by an overall constant different from $1$? I guess that would slightly change other parts of the analysis, but maybe we should at least comment on this?}

Thank you for this question. We cannot rule out the possibility of cancelations in the denominator of the quantity $q$ in Lemma 11 (and correspondingly, in the state of (4.101), which is now (4.104) in the revised version). If this quantity is small, then the complexity of our state preparation procedure is large. However, since the terms would have to be carefully adjusted to cancel, we suspect that such a situation is unlikely to present an obstacle in practice. Furthermore, for particular cases that do exhibit such cancelations, it is likely possible to adjust the linear system to reduce the cancelations, giving a smaller complexity. However, the details of such a procedure could depend heavily on the specific problem, and are unable to provide a stronger result for the general case at present.

\begin{lstlisting}
* There's a typo (adative -> adaptive) in Section 5. More generally, the discussion would benefit from more backreferences, e.g. to Table 1, as well as a bit more context (partially mirroring the introduction) on QLSAs with \polylog(1/\epsilon) scaling. Also, are there potential applications of the methods of this paper to other contexts or use cases?
\end{lstlisting}

Thanks for catching this typo. We changed ``adative" $\to$ ``adaptive" in Section 5.

\begin{lstlisting}
* In Appendix B, the second inequality of (B.1) is only true for n >= 4. I doubt this affects the asymptotic results, but the inequality is stated as though it's fully general. Could the authors double-check this?
\end{lstlisting}

We added the condition $n\ge4$ in (B.1) and correspondingly Lemma 7, Corollary 1, and Lemma 10. This indeed does not affect the asymptotic results.

\end{document}
