% !TEX root = ../multi-pde-draft.tex

\section{Singular values of second-order differential matrices}
\label{app:svd}

Here we present a detailed proof of the singular value estimation in \lem{svd_Fourier} and \lem{svd_Chebyshev}.

\svdFourier*

\begin{proof}
By direct calculation of the $l_{\infty}$ norm (i.e., the maximum absolute column sum) of \eq{matrix_Fourier}, we have
\begin{equation}
\|\overline{D}^{(2)}_n\|_{\infty} \le \left(\frac{(n+1)\pi}{2}\right)^2 \le (2n)^2.
\end{equation}

Then the inverse of the matrix \eq{matrix_Fourier} is
\begin{equation}
\begin{aligned}
&[(\overline{D}^{(2)}_n)^{-1}]_{k,k}=-\frac{1}{((k-\left\lfloor n/2 \right\rfloor)\pi)^{2}},\quad &k \in\rangez{n+1}\backslash\{\left\lfloor n/2 \right\rfloor\},\\
&[(\overline{D}^{(2)}_n)^{-1}]_{\left\lfloor n/2 \right\rfloor,k}=\frac{1}{((k-\left\lfloor n/2 \right\rfloor)\pi)^{2}},\quad &k \in\rangez{n+1}
\end{aligned}
\end{equation}
as can easily be verified by a direct calculation.

By direct calculation of the Frobenius norm of \eq{matrix_Fourier}, we have
\begin{equation}
\|(\overline{D}^{2}_n)^{-1}\|_F^2 \le 1+2\sum_{k=1}^{\infty}\frac{1}{k^4\pi^4} = 1+\frac{2}{\pi^4}\frac{\pi^4}{90} \le 2.
\end{equation}

Thus we have the result in \eq{svd_Fourier}:
\begin{equation}
\begin{aligned}
&\sigma_{\max}(\overline{D}^{(2)}_n) \le \sqrt{n+1}\|\overline{D}^{2}_n\|_{\infty} \le (2n)^{2.5},\\
&\sigma_{\min}(\overline{D}^{(2)}_n) \ge \frac{1}{\|(\overline{D}^{2}_n)^{-1}\|_F} \ge \frac{1}{\sqrt{2}}
\end{aligned}
\end{equation}
as claimed.
\end{proof}

\svdChebyshev*

\begin{proof}
By direct calculation of the Frobenius norm of \eq{matrix_Chebyshev}, we have
\begin{equation}
\|\overline{D}^{(2)}_n\|_F^2 \le n^2\max_{k,r}\left(\frac{r(r^2-k^2)}{\sigma_k}\right)^2 \le n^2\cdot n^6 = n^8.
\end{equation}

Next we upper bound $\|(\overline{D}^{(2)}_n)^{-1}\|$. By definition,
\begin{equation}
\|(\overline{D}^{(2)}_n)^{-1}\|=\sup_{\|b\|\le1}\|(\overline{D}^{2}_n)^{-1}b\|.
\end{equation}
Given any vector $b$ satisfying $\|b\|\le1$, we estimate $\|x\|$ defined by the full-rank linear system
\begin{equation}
\overline{D}^{(2)}_n x = b.
\label{eq:svd_system}
\end{equation}
Notice that $\overline{D}^{(2)}_n$ is the sum of the upper triangular matrix ${D}^{2}_n $ and \eq{Chebyshev_Gn}, the coordinates $x_2,\ldots,x_n$ are only defined by coordinates $b_0,\ldots,b_{n-2}$. So we only focus on the partial system
\begin{equation}
{D}^{(2)}_n [0,0,x_2,\ldots,x_n]^T = [b_0,\ldots,b_{n-2},0,0]^T.
\end{equation}
Given the same $b$, we also define the vector $y$ by
\begin{equation}
{D}_n [0,y_1,\ldots,y_{n-1},0]^T = [b_0,\ldots,b_{n-2},0,0]^T,
\end{equation}
where each coordinate of $y$ can be expressed by
\begin{equation}
b_k = \sum_{l=1}^{n-1}[{D}_n]_{kl}y_l = \sum_{l=1}^{n-1}\frac{2l}{\sigma_k}y_l, \quad \text{$k+l$ odd},~ l>k,~k\in\rangez{n-1}.
\label{eq:svd_by}
\end{equation}
Using this equation with $k=l-1$ and $k=l+1$, we can express $y_l$ in terms of $b_{l-1}$ and $b_{l+1}$:
\begin{equation}
\frac{2l}{\sigma_{l-1}} y_l = b_{l-1} - \frac{1}{\sigma_{l-1}} b_{l+1}, \quad l \in\range{n-1},
\label{eq:svd_yb}
\end{equation}
where we let $b_{n-1}=b_n=0$. Thus we have
\begin{equation}
\begin{aligned}
\sum_{l=1}^{n-1}y_l^2 
&= \sum_{l=1}^{n-1} \left(\frac{\sigma_{l-1}}{2l}\left(b_{l-1} - \frac{1}{\sigma_{l-1}} b_{l+1}\right)\right)^2 \\
&\le \sum_{l=1}^{n-1}\frac{\sigma_{l-1}^2}{4l^2}\left(1+\frac{1}{\sigma_{l-1}^2}\right)(b_{l-1}^2+b_{l+1}^2) \\
&\le \frac{5}{4}(b_0^2+b_2^2)+\frac{1}{16}\sum_{l=2}^{n-2}(b_{l-1}^2+b_{l+1}^2) \\
&\le 2\sum_{l=0}^{n-2}b_l^2.
\label{eq:svd_y2b2}
\end{aligned}
\end{equation}
We notice that $y$ also satisfies
\begin{equation}
[0,y_1,\ldots,y_{n-1},0]^T = {D}_n [0,0,x_2,\ldots,x_n]^T,
\end{equation}
where each coordinate of $y$ can be expressed by
\begin{equation}
y_l = \sum_{r=1}^n[{D}_n]_{lr}x_r = \sum_{r=1}^n\frac{2r}{\sigma_l}x_r, \quad \text{$l+r$ odd},~ r>l,~l\in\range{n-1}.
\label{eq:svd_yx}
\end{equation}
Substituting the $(r-1)$st and the $(r+1)$st equations of \eq{svd_yx}, we can express $x$ in terms of $y$:
\begin{equation}
\frac{2r}{\sigma_{r-1}} x_r = y_{r-1} - \frac{1}{\sigma_{r-1}} y_{r+1}, \quad r \in\range{n} \backslash \{1\},
\label{eq:svd_xy}
\end{equation}
where we let $y_n=y_{n+1}=0$. Similarly, according to \eq{svd_xy}, we also have
\begin{equation}
\sum_{l=2}^nx_l^2\le2\sum_{l=1}^{n-1}y_l^2.
\label{eq:svd_x2y2}
\end{equation}
Then we calculate $x_0^2+x_1^2$ based on the last two equations of \eq{svd_system}, \eq{svd_y2b2}, and \eq{svd_xy}, giving
\begin{equation}
\begin{aligned}
x_0^2+x_1^2 
&= \frac{1}{2}[(x_0+x_1)^2+(x_0-x_1)^2]\\
&= \frac{1}{2}\left[\left(b_n-\sum_{l=2}^nx_l\right)^2+\left(b_{n-1}-\sum_{l=2}^n(-1)^lx_l\right)^2\right] \\
&= \frac{1}{2}\left[\left(b_n-\sum_{l=2}^n\frac{\sigma_{l-1}}{2l}\left(y_{l-1} - \frac{1}{\sigma_{l-1}} y_{l+1}\right)\right)^2+\left(b_{n-1}-\sum_{l=2}^n(-1)^l\frac{\sigma_{l-1}}{2l}\left(y_{l-1} - \frac{1}{\sigma_{l-1}} y_{l+1}\right)\right)^2\right] \\
&\le \frac{1}{2}\left(1+\sum_{l=2}^n\frac{\sigma_{l-1}^2}{4l^2}\right)\left[b_n^2+\sum_{l=2}^n\left(y_{l-1} - \frac{1}{\sigma_{l-1}} y_{l+1}\right)^2+b_{n-1}^2+\sum_{l=2}^n\left(y_{l-1} - \frac{1}{\sigma_{l-1}} y_{l+1}\right)^2\right] \\
&\le \frac{1}{2}\left(1+\frac{1}{4}\sum_{l=2}^{\infty}\frac{1}{l^2}\right)\left[b_n^2+b_{n-1}^2+\sum_{l=2}^{n}\left(1+\frac{1}{\sigma_{l-1}^2}\right)(y_{l-1}^2+y_{l+1}^2)\right] \\
&\le \frac{1}{2}\left(1+\frac{\pi^2}{24}\right)\left[b_n^2+b_{n-1}^2+4\sum_{l=1}^{n-1}y_l^2\right] \\
&\le b_n^2+b_{n-1}^2+8\sum_{l=0}^{n-2}b_l^2.
\label{eq:svd_x0x1}
\end{aligned}
\end{equation}
Thus, based on \eq{svd_y2b2}, \eq{svd_x2y2}, and \eq{svd_x0x1}, the inequality
\begin{equation}
\begin{aligned}
\sum_{l=0}^nx_l^2 &= x_0^2+x_1^2+\sum_{l=2}^nx_l^2 \\
&\le b_n^2+b_{n-1}^2+8\sum_{l=0}^{n-2}b_l^2+4\sum_{l=0}^{n-2}b_l^2 \\
&\le b_n^2+b_{n-1}^2+12\sum_{l=0}^{n-2}b_l^2 \le 12
\end{aligned}
\end{equation}
holds for any vectors $b$ satisfying $\|b\|\le1$. Thus
\begin{equation}
\|(\overline{D}^{(2)}_n)^{-1}\| = \sup_{\|b\|\le1} \|x\| \le 12 < 16.
\end{equation}

Altogether, we have
\begin{equation}
\begin{aligned}
&\sigma_{\max}(\overline{D}^{(2)}_n) \le \|\overline{D}^{2}_n\|_F \le n^4,\\
&\sigma_{\min}(\overline{D}^{(2)}_n) \ge \frac{1}{\|(\overline{D}^{2}_n)^{-1}\|} \ge \frac{1}{16}
\end{aligned}
\end{equation}
as claimed in \eq{svd_Chebyshev}.
\end{proof}
