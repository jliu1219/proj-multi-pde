% !TEX root = ../multi-pde-draft.tex

\subsection{Main result}
\label{sec:main}

Having analyzed the condition number and state preparation procedure for our approach, we are now ready to establish the main result.

\begin{theorem}\label{thm:main}
Consider an instance of the quantum PDE problem as defined in \prb{pde} with Dirichlet boundary conditions \eq{dbc}. Then there exists a quantum algorithm that produces a state in the form of \eq{u_expand} whose amplitudes are proportional to $u(\bm{x})$ on a set of interpolation nodes $\bm{x}$ (with respect to the uniform grid nodes for periodic boundary conditions or the Chebyshev-Gauss-Lobatto quadrature nodes for non-periodic boundary conditions, as defined in in \eq{interpolation_nodes}), where $u(\bm{x})/\| u(\bm{x})\|$ is $\epsilon$-close to $\hat{u}(\bm{x})/\| \hat{u}(\bm{x})\|$ in $l_2$ norm for all nodes $\bm{x}$, succeeding with probability $\Omega(1)$, with a flag indicating success, using
\begin{equation}
  \biggl(\frac{d\|A\|_\Sigma}{C\|A\|_{\ast}}+qd^2\biggr)\poly(\log(g'/g\epsilon))
\end{equation}
queries to oracles as defined in \sec{state}.
Here $\|A\|_\Sigma:=\sum_{\|\bm{j}\|_1\le h}\|A_{\bm{j}}\|$, \diff{$\|A\|_{\ast}:=\sum_{j=1}^d |A_{j,j}|$,} $C$ is defined in \eq{DDM}, and
\begin{align}
g = \min_{\bm{x}} \|\hat{u}(\bm{x})\|, \qquad g':=\max_{\bm{x}} \max_{n \in \N}\|\hat{u}^{(n+1)}(\bm{x})\|,\qquad \\
q = \sqrt{\frac{\sum_{\|\bm{k}\|_{\infty}\le n} \sum_{j=1}^d \hat{f}^2_{\bm{k}}+(A_{j,j}\hat{\gamma}^{j+}_{\bm{k}})^2+(A_{j,j}\hat{\gamma}^{j-}_{\bm{k}})^2}{\sum_{\|\bm{k}\|_{\infty}\le n} \sum_{j=1}^d (\hat{f}_{\bm{k}}+A_{j,j}\hat{\gamma}^{j+}_{\bm{k}}+A_{j,j}\hat{\gamma}^{j-}_{\bm{k}})^2}}
.
\label{eq:main_thm_parameters}
\end{align}
The gate complexity is larger than the query complexity by a factor of $\poly(\log(d\|A\|_\Sigma/\epsilon))$.
\end{theorem}

\begin{proof}
We analyze the complexity of the algorithm presented in \sec{solver}.

First we choose
\begin{equation}
n:=\biggl\lfloor\frac{\log(\Omega)}{\log(\log(\Omega))}\biggr\rfloor,
\label{eq:truncated_number}
\end{equation}
where
\begin{equation}
\Omega=\frac{g'(1+\epsilon)}{g\epsilon}.
\label{eq:Omega}
\end{equation}
By Eq.~(1.8.28) of \cite{STW11}, this choice guarantees
\begin{equation}
\|\hat{u}(\bm{x})-u(\bm{x})\|\le \max_{t\in[0,T]}\|\hat{u}^{(n+1)}(\bm{x})\|\frac{e^{n}}{(2n)^n}\le\frac{g'}{\Omega}=\frac{g\epsilon}{1+\epsilon}=:\delta.
\label{eq:n_inequality}
\end{equation}
Now $\|\hat{u}(\bm{x})-u(\bm{x})\|\le\delta$ implies
\begin{equation}
\biggl\|\frac{\hat{u}(\bm{x})}{\|\hat{u}(\bm{x})\|}-\frac{u(\bm{x})}{\|u(\bm{x})\|}\biggr\|
\le\frac{\delta}{\min\{\|\hat{u}(\bm{x})\|,\|u(\bm{x})\|\}}\le\frac{\delta}{g-\delta}=\epsilon,
\label{eq:normalized_inequality}
\end{equation}
so we can choose $n$ to ensure that the normalized output state is $\epsilon$-close to $\hat{u}(\bm{x})/\| \hat{u}(\bm{x})\|$.

As described in \sec{solver}, the algorithm uses the high-precision QLSA from reference \cite{CKS15} and the multi-dimensional QSFT/QCT (and its inverse). According to \lem{qsft} and \lem{qct}, the $d$-dimensional (inverse) QSFT or QCT can be performed with gate complexity $O(d\log n\log\log n)$. According to \lem{preparation}, the query and gate complexity for state preparation is $O(qd^2\log n\log\log n)$.

For the linear system $L \vec x=\vec f + \vec g$ in \eq{linear_system}, the matrix $L$ is an $(n+1)^d \times (n+1)^d$ matrix with $(n+1)$ or $(n+1)d$ nonzero entries in any row or column for periodic or non-periodic conditions, respectively. According to \lem{condition}, the condition number of $L$ is upper bounded by $\frac{\|A\|_\Sigma}{C\|A\|_{\ast}}(2n)^4$. Consequently, by Theorem 5 of \cite{CKS15}, the QLSA produces a state proportional to $\vec x$ with $O(\frac{d\|A\|_\Sigma}{C\|A\|_{\ast}}(2n)^5)$ queries to the oracles, and its gate complexity is larger by a factor of $\poly(\log(d\|A\|_\Sigma\,n))$. Using the value of $n$ specified in \eq{truncated_number}, the overall query complexity of our algorithm is
\begin{equation}
\biggl(\frac{d\|A\|_\Sigma}{C\|A\|_{\ast}}+qd^2\biggr)\poly(\log(g'/g\epsilon)),
\end{equation}
and the gate complexity is
\begin{equation}
\biggl(\frac{d\|A\|_\Sigma}{C\|A\|_{\ast}}\poly(\log(d\|A\|_\Sigma/\epsilon))+qd^2\biggr)\poly(\log(g'/g\epsilon))
\end{equation}
which is larger by a factor of $\poly(\log(d\|A\|_\Sigma/\epsilon))$, as claimed.
\end{proof}

Note that we can establish a more efficient algorithm in the special case of the Poisson equation with homogenous boundary conditions. In this case, \diff{$\|A\|_\Sigma = \|A\|_{\ast} = d$ and $C=1$}. Under homogenous boundary conditions, the complexity of state preparation can be reduced to $d\poly(\log(g'/g\epsilon))$, since we can remove $2d$ applications of the QSFT or QCT for preparing a state depending on the boundary conditions, and since $\gamma=0$ there is no need to postselect on the uniform superposition to incorporate the boundary conditions. In summary, the query complexity of the Poisson equation with homogenous boundary conditions is
\begin{equation}
d\poly(\log(g'/g\epsilon));
\end{equation}
again the gate complexity is larger by a factor of
$\poly(\log(d\|A\|_\Sigma/\epsilon))$.
