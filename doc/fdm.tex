% !TEX root = ../multi-pde-draft.tex

\section{Finite difference method}
\label{sec:fdm}

We now describe our first approach to quantum algorithms for linear PDEs, based on the finite difference method (FDM). We first construct the linear system corresponding to the finite difference approximation of Poisson's equation with periodic boundary conditions (\sec{fdm_system}). Then we bound its condition number (\sec{fdm_cnum}) and the error of approximation (\sec{fdm_error}), and we use these results to give an efficient quantum algorithm (\sec{fdm_alg}). We conclude by discussing how to use the method of images to apply this algorithm for Neumann and Dirichlet boundary conditions (\sec{fdm_images}).

The FDM approximates the derivative of a function $f$ at a point $\bm{x}$ in terms of the values of $f$ on a finite set of points near $\bm{x}$.
Generally there are no restrictions on where these points are located relative to $\bm{x}$, but they are typically taken to be uniformly spaced points with respect to a certain coordinate. This corresponds to discretizing $[-1,1]^d$ (or $[0,2 \pi )^d$) to a $d$-dimensional rectangular lattice (where we use periodic boundary conditions).

For a scalar field, in which $u(\bm{x}) \in \C$, the canonical elliptic PDE is Poisson's equation \eq{Poisson},
% \begin{align}
%   \Biggl(\sum_{j=1}^{d} \frac{\partial^2}{\partial x_j^2}\Biggr) u(\bm{x}) = f(\bm{x}),
% \end{align}
which we consider solving on $[0, 2\pi)^d$ with periodic boundary conditions. This also implies results for
the domain $\Omega = [-1,1]^d$ under Dirichlet ($u(\partial \Omega )=0$) and Neumann ($\hat n \cdot \nabla u(\partial \Omega) =0$ where $\hat n$ denotes the normal direction to $\partial \Omega$, which for domain $\Omega=[-1,1]^d$ is equivalent to $\frac{\partial u}{\partial x_j}\big|_{x_j=\pm 1}=0$ for $j \in [d]$) boundary conditions.

\subsection{Linear system}
\label{sec:fdm_system}

To approximate the second derivatives appearing in Poisson's equation, we apply the central finite difference formula of order $2k$. Taking $x_j=j h$ for a lattice with spacing $h$, this formula gives the approximation
\begin{align}
f''(0) \approx \frac{1}{h^2} \sum_{j=-k}^{k} r_j f(jh)
\end{align}
where the coefficients are \cite{kivlichan2017bounding,li2005general}
\begin{align}
  r_j:=
  \begin{cases}
    \frac{2(-1)^{j+1} (k!)^2}{j^2 (k-j)!(k+j)!} & j \in [k]\\
    -2 \sum_{j=1}^{k} r_j & j=0 \\
    r_{-j} & j \in -[k].
  \end{cases}
  \label{eq:fdm_coeffs}
\end{align}
We leave the dependence on $k$ implicit in this notation. The following lemma characterizes the error of this formula.

\begin{lemma}[{\cite[Theorem 7]{kivlichan2017bounding}}] Let $k\ge 1$ and suppose $f(x) \in C^{2k+1}$ for $x \in \mathbb{R}$. Define the coefficients $r_j$ as in \eq{fdm_coeffs}. Then
\begin{align}
  \frac{\d^2 u(x_0) }{\d x^2} =
 \frac{1}{h^2} \sum_{j=-k}^{k} r_j f(x_0+jh) + O\Bigl( \Bigl|\frac{\d^{2k+1} u}{\d x^{2k+1}}\Bigr| \Bigl(\frac{eh}{2}\Bigr)^{2k-1}\Bigr)
\end{align}
where
\begin{align}
  \Bigl|\frac{\d^{2k+1} u}{\d x^{2k+1}}\Bigr| := \max_{y \in [x_0-kh,x_0+kh] } \Bigl|\frac{\d^{2k+1} u}{\d x^{2k+1}} (y)\Bigr|.
\end{align}
\end{lemma}

Since we assume periodic boundary conditions and apply the same FDM formula at each lattice site, the matrices we consider are circulant.
Define the $2n \times 2n$ matrix $S$ to have entries $S_{i,j} = \delta_{i,j+1 \bmod 2n} $.
If we represent the solution $u(x)$ as a vector $\vec u = \sum_{j=1}^{2n} u( \pi j / n) \vec e_j$, then we can approximate Poisson's equation using a central difference formula as
\begin{align}
\frac{1}{h^2 }L \vec u = \frac{1}{h^2} \Bigl(r_0 I + \sum_{j=1}^{k} r_j (S^j + S^{-j})\Bigr) \vec u = \vec f
\end{align}
where $\vec f = \sum_{j=1}^{2n} f( \pi j / n) \vec e_j$. The solution $\vec u$ corresponds exactly with the quantum state we want to produce, so we do not have to perform any post-processing such as in Ref.~\cite{CJO17} and other quantum differential equation algorithms. The matrix in this linear system is just the finite difference matrix, so it suffices to bound its condition number and approximation error (whereas previous quantum algorithms involved more complicated linear systems).

\subsection{Condition number}
\label{sec:fdm_cnum}

The following lemma characterizes the condition number of a circulant Laplacian on $2n$ points.

\begin{lemma} \label{lem:condnumpbc} For $k = o(n^{2/3})$, the matrix $L = r_0 I + \sum_{j=1}^{k} r_j (S^j + S^{-j})$ with $r_j$ as in \eq{fdm_coeffs} has condition number $\kappa (L) = \Theta (n^2)$.
\end{lemma}
\begin{proof}
% We directly consider $L$'s eigenvalues since it is symmetric so its singular values are the absolute values of its eigenvalues. \amc{maybe this is obvious enough that we don't have to say it}

We first upper bound $\norm{L}$ using Gershgorin's circle theorem \cite{horn2012matrix} (a similar argument appears in Ref.~\cite{kivlichan2017bounding}).
Note that
\begin{align}
  |r_j| = \frac{2(k!)^2}{j^2 (k-j)!(k+j)!} \leq \frac{2}{j^2}
\end{align}
since
\begin{align}
  \frac{(k!)^2}{(k-j)!(k+j)!} = \frac{k(k-1) \cdots (k-j+1)}{(k+j)(k+j-1) \cdots (k+1)}<1.
\end{align}
The radii of the Gershgorin discs are
\begin{align}
2 \sum_{j=1}^{k} |r_j| & \leq 2 \sum_{j=1}^{k} \frac{2}{j^2}  \leq \frac{2\pi^2}{3}.
\end{align}
The discs are centered at $r_0$, and
\begin{align}
|r_0| & \leq 2 \sum_{j=1}^{k} |r_j|  \leq \frac{2\pi^2}{3},
\end{align}
so $\norm{L} \leq \frac{4\pi^2}{3}$.

To lower bound $\norm{L^{-1}}$ we lower bound the (absolute value of the) smallest non-zero eigenvalue of $L$ (since by construction the all-ones vector is a zero eigenvector). Let $\omega := \exp ( \pi i /n)$. Since $L$ is circulant, its eigenvalues are
\begin{align}
\lambda_l  & = r_0 + \sum_{j=1}^{k} r_j (\omega^{lj} + \omega^{-lj})\\
& = r_0 + \sum_{j=1}^{k} 2 r_j \cos \Bigl(\frac{\pi l j }{n}\Bigr)\\
& = r_0 + \sum_{j=1}^{k} 2 r_j \left( 1 - \frac{\pi^2 l^2 j^2}{2n^2} + \frac{(\pi c_j)^4}{4!n^4} \cos \left(\frac{\pi c_j }{n}\right) \right) \\
& =  - \frac{ \pi^2 l^2}{n^2} \sum_{j=1}^{k}  r_j j^2 + O \left(  \frac{l^4k^3}{n^4}  \right)
\end{align}
where the $c_j \in [0, lj]$  arise from the Taylor remainder theorem. The last line follows from the fact that $|r_j| = O(1/ j^2)$.

We now compute the sum
\begin{align}
-\sum_{j=1}^{k}  r_j j^2  & = \sum_{j=1}^{k}  j^2  \frac{2(-1)^j (k!)^2}{j^2 (k+j)! (k-j)!} \\
& =2 (k!)^2 \sum_{j=1}^{k} \frac{(-1)^j }{ (k+j)! (k-j)!} \\
&= \frac{2 (k!)^2}{(2k)!} \sum_{j=1}^{k} (-1)^j \binom{2k}{k+j} \\
&= \frac{2 (k!)^2}{(2k)!} \sum_{j=k+1}^{2k} (-1)^{j+k} \binom{2k}{j} \\
& = (-1)^k \frac{(k!)^2}{(2k)!} \sum_{j=0,\, j \neq k}^{2k} (-1)^{j} \binom{2k}{j} \\
& =(-1)^k \frac{(k!)^2}{(2k)!} \biggl((1-1)^{2k}- (-1)^k \binom{2k}{k}\biggr) \\
& = -1.
\end{align}

In particular, we have
\begin{align}
% \lambda_l & =- \frac{ \pi^2 l^2}{n^2} + O \left(  \frac{l^4k^3}{n^4}  \right) \\
\lambda_1 & =- \frac{ \pi^2}{n^2} + O\biggl( \frac{k^3}{n^4} \biggr)
\end{align}
and the error term is vanishingly small for $k = o(n^{2/3})$.
\end{proof}

In $d$ dimensions, a similar analysis holds.

\begin{lemma} \label{lem:cnum_higherd} For $k = o(n^{2/3})$, let $L := r_0 I + \sum_{j=1}^{k} r_j (S^j + S^{-j})$ with $r_j$ as in \eq{fdm_coeffs}. The matrix $L' := L \otimes I^{\otimes d-1} + I \otimes L \otimes I^{\otimes d-2} + \dots + I^{\otimes d-1} \otimes L$ has condition number $\kappa (L') = \Theta (dn^2)$.
\end{lemma}

\begin{proof}
By the triangle inequality for spectral norms, $\norm{L'} \leq d \norm{L}$.
Since $L$ has zero-sum rows by construction, the all-ones vector lies in its kernel, and thus the smallest non-zero eigenvalue of $L$ is the same as that of $L'$.
\end{proof}

\subsection{Error analysis}
\label{sec:fdm_error}

There are two types of error relevant to our analysis: the FDM error and the QLSA error. We assume that we are able to perfectly generate states proportional to $\vec f$.
The FDM errors arise from the remainder terms in the finite difference formulas and from inexact approximations of the eigenvalues.

We introduce several states for the purpose of error analysis.
Let $|u \rangle$ be the quantum state that is proportional to $\vec u = \sum_{j \in \mathbb{Z}_{2n}^d } u(\pi j/n) \bigotimes_{i=1}^d e_{j_i}$ for the exact solution of the differential equation.
Let $| \bar u \rangle$ be the state output by a QLSA that exactly solves the linear system.
Let $| \tilde u \rangle$ be the state output by a QLSA with error.
Then the total error of approximating $|u \rangle $ by $| \tilde u \rangle$ is bounded by
\begin{align}
\norm{| u \rangle -| \tilde u \rangle }
& \leq \norm{| u \rangle -| \bar  u \rangle } + \norm{| \bar u \rangle -| \tilde u \rangle } \\
& = \epsilon_{\mathrm{FDM}} + \epsilon_{\mathrm{QLSA}}
\end{align}
\noindent and without loss of generality we can take $\epsilon_{\mathrm{FDM}}$ and $\epsilon_{\mathrm{QLSA}}$ to be of the same order of magnitude.

\begin{lemma} Let $u(\vec x)$ be the exact solution of $( \sum_{i=1}^d \frac{\d^2 }{\d x_i^2}) u (\vec x) = f(\vec x) $.
Let $\vec u \in \mathbb{R}^{(2n)^d}$ encode the exact solution in the sense that $\vec u = \sum_{j \in \mathbb{Z}_{2n}^d } u(\pi j/n) \bigotimes_{i=1}^d e_{j_i}$.
Let $\bar u \in \mathbb{R}^{(2n)^d}$ be the exact solution of the FDM linear system $\frac{1}{h^2} L' \bar u = \vec f$, where $L'$ is a $d$-dimensional $(2k)$th-order Laplacian as above where $k = o(n^{2/3})$ and $\vec f = \sum_{j=1}^{2n} f( \pi j / n) \vec e_j$.
Then $\norm{\vec u - \bar u} \leq O( 2^{d/2} n^{(d/2)-2k+1} \bigl| \frac{\d^{2k+1} u}{\d x^{2k+1}} \bigr| (e^2/4)^k)$.
\end{lemma}

\begin{proof}
The remainder term of the central difference formula is $O( \bigl| \frac{\d^{2k+1} u}{\d x^{2k+1}} \bigr| h^{2k-1} (e/2)^{2k} )$, so
\begin{align}
% \frac{1}{h^2} L' \bar u & = \vec f \\
\frac{1}{h^2} L' \vec u & = \vec f + O\Bigl( \Bigl| \frac{\d^{2k+1} u}{\d x^{2k+1}} \Bigr| (eh/2)^{2k-1}\Bigr)  \vec \epsilon
\end{align}
where $\vec \epsilon$ is a $(2n)^d$ dimensional vector whose entries are $O(1)$. This implies
\begin{align}
\frac{1}{h^2} L' (\vec u - \bar u) & =O\Bigl( \Bigl| \frac{\d^{2k+1} u}{\d x^{2k+1}} \Bigr| (eh/2)^{2k-1} \Bigr) \vec \epsilon
\end{align}
and therefore
\begin{align}
 \norm{\vec u - \bar u} & =O\Bigl( \Bigl| \frac{\d^{2k+1} u}{\d x^{2k+1}} \Bigr| (eh/2)^{2k+1} \Bigr) \norm{(L')^{-1} \vec \epsilon} \\
 & =O\Bigl( (2n)^{d/2}  \Bigl| \frac{\d^{2k+1} u}{\d x^{2k+1}} \Bigr| (eh/2)^{2k+1} / \lambda_1\Bigr).
\end{align}

By \lem{condnumpbc} we have $\lambda_1 = \Theta (1/n^2)$, and since $h = \Theta (1/n)$, we have
\begin{align}
 \norm{\vec u - \bar u}
 &=O \Bigl( 2^{d/2} n^{(d/2)-2k+1}  \Bigl| \frac{\d^{2k+1} u}{\d x^{2k+1}} \Bigr| (e/2)^{2k}\Bigr)
\end{align}
as claimed.
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{FDM algorithm}
\label{sec:fdm_alg}

To apply QLSAs, we must consider the complexity of simulating Hamiltonians that correspond to Laplacian FDM operators.
For periodic boundary conditions, the Laplacians are circulant, so they can be diagonalized by the QFT $F$ (or a tensor product of QFTs for the multidimensional Laplacian $L'$), i.e., $D=F^{\dagger}LF$ is diagonal. In this case the simplest way to simulate $\exp (i Lt)$ is to perform the inverse QFT, apply controlled phase rotations to implement $\exp (iDt)$, and perform the QFT.
Reference~\cite{schuch2003programmable} shows how to exactly implement arbitrary diagonal unitaries on $m$ qubits using $O(2^m)$ gates. Since we consider Laplacians on $n$ lattice sites, simulating $\exp (iLt)$ takes $O(n)$ gates with the dominant contribution coming from the phase rotations (alternatively, the methods of Ref.~\cite{welch2014efficient} or Ref.~\cite{berry2015simulating} could also be used).
Using this Hamiltonian simulation algorithm in a QLSA for the FDM linear system gives us the following theorem.

\begin{theorem}(Quantum FDM for Poisson equation with periodic boundary conditions)
There exists a quantum algorithm that outputs a state $\epsilon$-close to $| u \rangle$ that runs in time
\begin{align}
\tilde O ( d^2\log^{9/2+ \gamma} ( \sqrt{d} \Bigl| \frac{\d^{2k+1} u}{\d x^{2k+1}} \Bigr| / \epsilon ) \sqrt{ \log (1/ \epsilon) } )
\end{align}
and makes
\begin{align}
\tilde O (d \log^{3+ \beta} ( \sqrt{d} \Bigl|\frac{\d^{2k+1} u}{\d x^{2k+1}} \Bigr| / \epsilon ) \sqrt{ \log (1/ \epsilon) } )
\end{align}
queries to the oracle for $\vec f$
for arbitrarily small $\gamma, \beta>0$.
\end{theorem}

\begin{proof}
We use the Fourier series based QLSA from Ref.~\cite{CKS15}.
By Theorem 3 of that work, the QLSA makes $O (\kappa \sqrt{ \log (\kappa / \epsilon_{\mathrm{QLSA}})} )$ uses of a Hamiltonian simulation algorithm and uses of the oracle for the inhomogeneity.
For Hamiltonian simulation we use $d$ parallel QFTs and phase rotations as described in Ref.~\cite{schuch2003programmable}, for a total of $O (dn\kappa \sqrt{\log (\kappa / \epsilon_{\mathrm{QLSA}})} )$ gates. The condition number for the $d$-dimensional Laplacian scales as $\kappa = O(dn^2)$.

We take $\epsilon_{\mathrm{FDM}}$ and $\epsilon_{\mathrm{QLSA}}$ to be of the same order and just write $\epsilon$. Then the QLSA has time complexity $O( n^{3} \sqrt{ \log (n^2/ \epsilon)} )$ and query complexity $O(n^2 \log (n^2 / \epsilon ) )$.
The adjustable parameters are the number of lattice sites $n$ and the order of the finite difference formula.
To keep the error below the target error of $\epsilon$ we require
\begin{align}
2^{d/2} n^{(d/2)-2k+1}  \Bigl| \frac{\d^{2k+1} u}{\d x^{2k+1}} \Bigr| (e/2)^{2k} & = O( \epsilon ),
\end{align}
% 2^{d/2} n^{(d/2)-2k+1} (e/2)^{2k} & = O( \epsilon /  \Bigl| \frac{\d^{2k+1} u}{\d x^{2k+1}} \Bigr| ) \\
% 2^{-d/2} n^{-(d/2)+2k-1} (e/2)^{-2k} & = \Omega ( \Bigl| \frac{\d^{2k+1} u}{\d x^{2k+1}} \Bigr| / \epsilon  ) \\
or equivalently,
\begin{align}
(-d/2) + (-(d/2)+2k-1) \log (n) -2k \log (e/2) & = \Omega \Bigl( \log \Bigl(\Bigl| \frac{\d^{2k+1} u}{\d x^{2k+1}} \Bigr| / \epsilon  \Bigr)\Bigr).
\end{align}
We can satisfy this condition by setting $k=dn^b$ for some constant $b<2/3$ (so that our condition number lemmas hold) and taking $n$ sufficiently large, giving
\begin{align}
% dn^b \log (n) & = \Omega ( \log (\Bigl| \frac{\d^{2k+1} u}{\d x^{2k+1}} \Bigr| / \epsilon  )) \\
n^b \log (n) & = \Omega \Bigl( \frac{1}{d} \log \Bigl(\Bigl| \frac{\d^{2k+1} u}{\d x^{2k+1}} \Bigr| / \epsilon  \Bigr)\Bigr).
\end{align}
We can set $n = \Theta ( \log^{3/2+ \delta} (\bigl| \frac{\d^{2k+1} u}{\d x^{2k+1}} \bigr| / \epsilon  ))$ to satisfy this criterion for arbitrary ($b$-dependent) $\delta >0$.
The QLSA
% runs in time $ \tilde O ( d^2\log^{9/2+ \gamma} ( \sqrt{d} \bigl| \frac{\d^{2k+1} u}{\d x^{2k+1}} \bigr| / \epsilon ) \sqrt{ \log (1/ \epsilon) } )$
 % and makes $ \tilde O (d \log^{3+ \beta} ( \sqrt{d} \bigl| \frac{\d^{2k+1} u}{\d x^{2k+1}} \bigr| / \epsilon ) \sqrt{ \log (1/ \epsilon) } )$ queries to the oracle for $\vec f$ for arbitrarily small $\gamma, \beta$.
then has the stated running time and query complexity.
\end{proof}

This can be compared to the cost of using the conjugate gradient method to solve the same linear system classically.
The sparse conjugate gradient algorithm for an $N \times N$ matrix has time complexity $O(Ns \sqrt{\kappa} \log (1 / \epsilon))$. For arbitrary dimension $N=\Theta (n^d)$, we have $s=dk=d^2n^b$ and $\kappa = O(dn^2)$, so that the time complexity is $ O( d^{2.5} \log (1/\epsilon) \log^{3+1.5d+1.5b + \gamma'} ( \sqrt{d} \bigl|\frac{\d^{2k+1} u}{\d x^{2k+1}} \bigr| / \epsilon ) )$
for arbitrary $\gamma'>0$.
Alternatively, $d$ fast Fourier transforms could be used, although this will generally take $\Omega (n^d) = \Omega (  \log^{3d/2 } ( \sqrt{d} \bigl|\frac{\d^{2k+1} u}{\d x^{2k+1}} \bigr| / \epsilon )) $ time.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Boundary conditions via the method of images}
\label{sec:fdm_images}

We can apply the method of images to deal with homogeneous Neumann and Dirichlet boundary conditions using the algorithm for periodic boundary conditions described above. In the method of images, the domain $[-1,1]$ is extended to include all of $\mathbb{R}$, and the boundary conditions are related to symmetries of the solutions.
For a pair of Dirichlet boundary conditions there are two symmetries: the solutions are anti-symmetric about $-1$ (i.e., $f(-x-1)=-f(x-1)$) and anti-symmetric about 1 (i.e., $f(1+x)= - f(1-x)$). Continuity and anti-symmetry about $-1$ and $1$ imply $f(-1)=f(1)=0$, and furthermore that $f(x)=0$ for all odd $x \in \mathbb{Z}$ and that $f(x+4)=f(x)$ for all $x \in \R$. For Neumann boundary conditions, the solutions are instead symmetric about $-1$ and $1$, which also implies $f(x+2)=f(x)$ for all $x \in \R$.

We would like to combine the method of images with the FDM to arrive at finite difference formulas for this special case.
In both cases, the method of images implies that the solutions are periodic, so without loss of generality we can consider a lattice on $[0, 2 \pi)$ instead of a lattice on $\mathbb{R}$.
It is useful to think of this lattice in terms of the cycle graph on $2n$ vertices, i.e., $(V,E)= ( \mathbb{Z}_{2n} , \{(i,i+1) \mid i \in \mathbb{Z}_{2n} \} )$, which means that the vectors encoding the solution $u(x)$ will lie in $\mathbb{R}^{2n}$. Let each vector $\vec e_j$ correspond to the vertex $j$. Then we divide $\mathbb{R}^{2n}$ into a symmetric and an anti-symmetric subspace, namely $\mathrm{span} \{ e_j + e_{2n+1-j} \}_{j=1}^{n} $ and $\mathrm{span} \{ e_j - e_{2n+1-j} \}_{j=1}^{n} $, respectively. Vectors lying in the symmetric subspace correspond to solutions that are symmetric about $0$ and $\pi$, so they obey Neumann boundary conditions at $0$ and $\pi$; similarly, vectors in the anti-symmetric space correspond to solutions obeying Dirichlet boundary conditions at $0$ and $\pi$.

Restricting to a subspace of vectors reduces the size of the FDM vectors and matrices we consider, and the symmetry of that subspace indicates how to adjust the coefficients.

If the FDM linear system is $L'' \vec u'' = \vec f''$ then $L''$ has entries
\begin{align}
L''_{i,j} =
\begin{cases}
r_{|i-j|} \pm r_{i+j-1} & i \leq k \\
r_{|i-j|} & k < i \leq n-k \\
r_{|i-j|} \pm r_{2n-i-j+1 } & n-k \leq i
\end{cases}
\end{align}
where $+$ ($-$) is chosen for Neumann (Dirichlet) boundary conditions and due to the truncation order $k$, $r_j=0$ for any $j > k$.
This is similar to how Laplacian coefficients are modified when imposing boundary conditions in discrete variable representations \cite{colbert1992novel}.

For the purpose of solving the new linear systems using quantum algorithms, we still treat these cases as obeying periodic boundary conditions. We assume access to an oracle that produces states $| f'' \rangle $ proportional to the inhomogeneity $f''(x)$. Then we apply the QLSA for periodic boundary conditions using $| f '' \rangle | \pm \rangle$ to encode the inhomogeneity, which will output solutions of the form $|u'' \rangle | \pm \rangle$. Here the ancillary state is chosen to be $| + \rangle$ ($| - \rangle$) for Neumann (Dirichlet) boundary conditions.

Typically, the (second-order) graph Laplacian for the path graph with Dirichlet boundary conditions has diagonal entries that are all equal to 2; however, using the above specification for the entries of $L$ leads to the $(1,1)$ and $(n,n)$ entries being 3 while the rest of the diagonal entries are 2.

To reproduce this case, we consider an alternative subspace restriction used in Ref.~\cite{Spielman} to diagonalize the Dirichlet graph Laplacian. In this case it is easiest to consider the lattice of a cycle graph on $2n+2$ vertices, where the vertices $0$ and $n+1$ are selected as boundary points where the field takes the value 0. The relevant antisymmetric subspace is now $\mathrm{span}(\{ e_j - e_{2n+2-j} \}_{j=1}^{n})$ (which has no support on $e_0$ and $e_{n+1}$).

If we again write the linear system as $L'' \vec u'' = \vec f''$, then the Laplacian has entries

\[
L''_{i,j} =
\begin{cases}
r_{|i-j|} - r_{i+j} & i \leq k \\
r_{|i-j|} & k < i \leq n-k \\
r_{|i-j|} - r_{2n-i-j+2 } & n-k \leq i.
\end{cases}
\]

We again assume access to an oracle producing states proportional to $f''(x)$; however, we assume that this oracle operates in a Hilbert space with one additional dimension compared to the previous approaches (i.e., whereas previously we considered implementing $U$, here we consider implementing $\Bigl(\begin{smallmatrix}
U & \vec 0 \\ \vec 0^T & 1
\end{smallmatrix}\Bigr)$). With this oracle we again prepare the state $|f'' \rangle |- \rangle$ and solve Poisson's equation for periodic boundary conditions to output a state $|u'' \rangle |- \rangle$ (where $| u'' \rangle$ lies in an ($n+1$)-dimensional Hilbert space but has no support on the $(n+1)$st basis state).
