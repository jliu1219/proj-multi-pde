% !TEX root = ../multi-pde-draft.tex

\subsection{Linear system}
\label{sec:solver}

In this section we introduce the quantum PDE solver for the problem \eq{pde}. We construct a linear system that encodes the solution of \eq{pde} according to the pseudo-spectral method introduced above, using the QSFT/QCT introduced in \sec{transform} to ensure sparsity.

We consider a linear PDE problem (\prb{pde}) with periodic boundary conditions
\begin{equation}
u(\bm{x}+2\bm{v}) = u(\bm{x}) \quad \forall\, \bm{x} \in \D, ~ \forall\, \bm{v} \in \Z^d
\label{eq:pbc}
\end{equation}
or non-periodic Dirichlet boundary conditions
\begin{equation}
u(\bm{x}) = \gamma(\bm{x}) \quad \forall\, \bm{x} \in \partial\D.
\label{eq:dbc}
\end{equation}
According to the elliptic regularity theorem (Theorem 6 in Section 6.3 of \cite{eve10}), there exists a unique solution $\hat{u}(\bm{x})$ in $C^\infty$ for \prb{pde}.

We now show how to apply the Fourier and Chebyshev pseudo-spectral methods to this problem. Our goal is to obtain the quantum state
\begin{equation}
  |u\rangle
% |u( \{ \bm{\chi}_{\bm{l}} \})\rangle
\propto \sum_{ \|\bm{k}\|_{\infty}, \|\bm{l}\|_{\infty}\le n} c_{\bm{k}}\phi_{\bm{k}}(\bm{\chi}_{\bm{l}})|l_1\rangle\ldots|l_d\rangle,
\label{eq:u_expand}
\end{equation}
where $\phi_{\bm{k}}(\bm{\chi}_{\bm{l}})$ is defined by \eq{basis_tensor} using \eq{basis_function} for the appropriate boundary conditions (periodic or non-periodic). This state corresponds to a truncated Fourier/Chebyshev approximation and is $\epsilon$-close to the exact solution $\hat{u}(\bm{\chi}_{\bm{l}})$ with $n=\poly(\log(1/\epsilon))$ \cite{STW11}. Note that this state encodes the values of the solution at the interpolation nodes \eq{interpolation_nodes} appropriate to the boundary conditions (the uniform grid nodes in the Fourier approach, for periodic boundary conditions, and the Chebyshev-Gauss-Lobatto quadrature nodes in the Chebyshev approach, for non-periodic boundary conditions).

Instead of developing our algorithm for the standard basis, we aim to produce a state
\begin{equation}
|c\rangle \propto \sum_{\|\bm{k}\|_{\infty}\le n} c_{\bm{k}}|k_1\rangle\ldots|k_d\rangle
\label{eq:u_coeff}
\end{equation}
that is the inverse QSFT/QCT of $|u\rangle$. We then apply the QSFT/QCT to transform back into the interpolation node basis.

The truncated spectral series of the inhomogeneity $f(\bm{x})$ and the boundary conditions $\gamma(\bm{x})$ can be expressed as
\begin{equation}
f(\bm{x}) = \sum_{\|\bm{k}\|_{\infty}\le n} \hat{f}_{\bm{k}}\phi_{\bm{k}}(\bm{x})
\label{eq:f_expand}
\end{equation}
and
\begin{equation}
\gamma(\bm{x}) = \sum_{\|\bm{k}\|_{\infty}\le n} \hat{\gamma}_{\bm{k}}\phi_{\bm{k}}(\bm{x}),
\label{eq:g_expand}
\end{equation}
respectively. We define quantum states $|f\rangle$ and $|\gamma\rangle$ by interpolating the nodes $\{\bm{\chi}_{\bm{l}}\}$ defined by \eq{interpolation_nodes} as
\begin{equation}
|f\rangle
% |f( \{ \bm{\chi}_{\bm{l}} \})\rangle
\propto \sum_{\|\bm{k}\|_{\infty},\|\bm{l}\|_{\infty}\le n} \phi_{k_j}(\bm{\chi}_{\bm{l}})\hat{f}_{\bm{k}}|l_1\rangle\ldots|l_d\rangle,
\label{eq:f_inter}
\end{equation}
and
\begin{equation}
  |\gamma\rangle
% |\gamma(\{ \bm{\chi}_{\bm{l}} \} )\rangle
\propto \sum_{\|\bm{k}\|_{\infty},\|\bm{l}\|_{\infty}\le n} \phi_{k_j}(\bm{\chi}_{\bm{l}})\hat{\gamma}_{\bm{k}}|l_1\rangle\ldots|l_d\rangle,
\label{eq:g_inter}
\end{equation}
respectively. These are the states that we assume we can produce using oracles. We perform the multidimensional inverse QSFT/QCT to obtain the states
\begin{equation}
|\hat{f}\rangle \propto \sum_{\|\bm{k}\|_{\infty}\le n} \hat{f}_{\bm{k}}|k_1\rangle\ldots|k_d\rangle,
\label{eq:f_coeff}
\end{equation}
and
\begin{equation}
|\hat{\gamma}\rangle \propto \sum_{\|\bm{k}\|_{\infty}\le n} \hat{\gamma}_{\bm{k}}|k_1\rangle\ldots|k_d\rangle.
\label{eq:g_coeff}
\end{equation}

Having defined these states, we now detail the construction of the linear system. At a high level, we construct two linear systems: one system $A \vec x = \vec f$ (where $\vec x$ corresponds to \eq{u_coeff}) describes the differential equation, and another system $B \vec x = \vec g$ describes the boundary conditions. We combine these into a linear system with the form
\begin{equation}
L \vec x =(A+B) \vec x = \vec f + \vec g.
\label{eq:linear_system}
\end{equation}
Even though we do not impose the two linear systems separately, we show that there exists a unique solution of \eq{linear_system} (which is therefore the solution of the simultaneous equations $A\vec x=\vec f$ and $B\vec x=\vec g$), since we show that $L$ has full rank, and indeed we upper bound its condition number in \sec{condition}.
% And the unique solution satisfies $A \vec x = \vec f$ and $B \vec x = \vec g$ respectively, since $rank(L) = rank(A) + rank (B)$.

Part of this linear system will correspond to just the differential equation
\begin{equation}
\L(u(\bm{\chi}_{\bm{l}}))=\sum_{\|\bm{j}\|_{1}= 2}A_{\bm{j}}\frac{\partial^{\bm{j}}}{\partial \bm{x}^{\bm{j}}}u(\bm{\chi}_{\bm{l}})=f(\bm{\chi}_{\bm{l}}),
\label{eq:inter_eq}
\end{equation}
while another part will come from imposing the boundary conditions on $\partial\D=\bigcup_{j \in [d]} \partial\D_j$, where $\partial\D_j:=\{\bm{x} \in \D \mid x_j=\pm1\}$ is a $(d-1)$-dimensional subspace.
More specifically, the boundary conditions
\begin{equation}
 u(\bm{\chi}_{\bm{l}}) = \gamma(\bm{\chi}_{\bm{l}}) \quad \forall\, \bm{\chi}_{\bm{l}} \in \partial\D
 \label{eq:proj_sample}
\end{equation}
can be expressed as conditions on each boundary:
\begin{equation}
\begin{aligned}
u(x_1,\ldots,x_{j-1},1,x_{j+1},\ldots,x_d)=\gamma^{j+},\quad \bm{x}\in \partial\D_j, \quad j \in [d]\\
u(x_1,\ldots,x_{j-1},-1,x_{j+1},\ldots,x_d)=\gamma^{j-},\quad \bm{x}\in \partial\D_j, \quad j \in [d].
\label{eq:proj_combination}
\end{aligned}
\end{equation}

\subsubsection{Linear system from the differential equation}
To evaluate the matrix corresponding to the differential operator from \eq{inter_eq}, it is convenient to define coefficients $c^{(\bm{j})}_{\bm{k}}$ and $\|\bm{k}\|_{\infty}\le n$ such that
\begin{equation}
\frac{\partial^{\bm{j}}}{\partial \bm{x}^{\bm{j}}}u(\bm{x})=\sum_{\|\bm{k}\|_{\infty}\le n}c^{(\bm{j})}_{\bm{k}}\phi_{\bm{k}}(\bm{x})
\label{eq:diff_expand}
\end{equation}
for some fixed $\bm{j} \in \N^d$ (as we explain below, such a decomposition exists for the choices of basis functions in \eq{basis_function}).
Using this expression, we obtain the following linear equations for $c^{(\bm{j})}_{\bm{k}}$:
\begin{equation}
\sum_{\|\bm{j}\|_{1}= 2}A_{\bm{j}}\sum_{\|\bm{k}\|_{\infty},\|\bm{l}\|_{\infty}\le n}\phi_{\bm{k}}(\bm{\chi}_{\bm{l}})c^{(\bm{j})}_{\bm{k}}|l_1\rangle\ldots|l_d\rangle=\sum_{\|\bm{k}\|_{\infty},\|\bm{l}\|_{\infty}\le n} \phi_{\bm{k}}(\bm{\chi}_{\bm{l}})\hat{f}_{\bm{k}}|l_1\rangle\ldots|l_d\rangle.
\label{eq:diff_coeff_system}
\end{equation}
To determine the transformation between $c^{(\bm{j})}_{\bm{k}}$ and $c_{\bm{k}}$, we can make use of the differential properties of Fourier and Chebyshev series, namely
\begin{align}
  \frac{\d}{\d x} e^{ik\pi x}=ik\pi e^{ik\pi x}
\end{align}
and
\begin{align}
  2T_k(t)=\frac{T'_{k+1}(t)}{k+1}-\frac{T'_{k-1}(t)}{k-1},
\end{align}
respectively.
We have
\begin{equation}
c^{(\bm{j})}_{\bm{k}}=\sum_{\|\bm{r}\|_{\infty}\le n}[\bm{D}^{(\bm{j})}_{n}]_{\bm{kr}}c_{\bm{r}.},\quad \|\bm{k}\|_{\infty}\le n,
\label{eq:diff_transform}
\end{equation}
where $\bm{D}^{(\bm{j})}_{n}$ can be expressed as the tensor product
\begin{equation}
\bm{D}^{(\bm{j})}_{n} = D^{j_1}_n \otimes D^{j_2}_n \otimes \cdots \otimes D^{j_d}_n,
\label{eq:diff_tensor}
\end{equation}
with $\bm{j}=(j_1,\ldots,j_d)$. The matrix $D_n$ for the Fourier basis functions in \eq{basis_function} can be written as the $(n+1)\times(n+1)$ diagonal matrix with entries
\begin{equation}
[D_n]_{kk}=i(k-\left\lfloor n/2 \right\rfloor)\pi.
\label{eq:Fourier_Dn}
\end{equation}
As detailed in Appendix A of \cite{CL19}, the matrix $D_n$ for the Chebyshev polynomials in \eq{basis_function} can be expressed as the $(n+1)\times(n+1)$ upper triangular matrix with nonzero entries
\begin{equation}
[D_n]_{kr}=\frac{2r}{\sigma_k},\qquad \text{$k+r$ odd},~ r>k,
\label{eq:Chebyshev_Dn}
\end{equation}
where
\begin{equation}
\sigma_k := \begin{cases}
2 & k=0\\
1 & k\in\range{n}.
\end{cases}
\end{equation}

Substituting \eq{diff_tensor} into \eq{diff_coeff_system}, with $D_n$ defined by \eq{Fourier_Dn} in the periodic case or \eq{Chebyshev_Dn} in the non-periodic case, and performing the multidimensional inverse QSFT/QCT (for a reason that will be explained in the next section), we obtain the following linear equations for $c_{\bm{r}}$:
\begin{equation}
\sum_{\|\bm{j}\|_1= 2}A_{\bm{j}}\sum_{\|\bm{k}\|_{\infty},\|\bm{l}\|_{\infty},\|\bm{r}\|_{\infty}\le n}[\bm{D}^{(\bm{j})}_{n}]_{\bm{kr}}c_{\bm{r}}|l_1\rangle\ldots|l_d\rangle=\sum_{\|\bm{k}\|_{\infty},\|\bm{l}\|_{\infty}\le n}\hat{f}_{\bm{k}}|l_1\rangle\ldots|l_d\rangle.
\label{eq:coeff_system}
\end{equation}

Notice that the matrices \eq{Fourier_Dn} and \eq{Chebyshev_Dn} are not full rank. More specifically, there exists at least one zero row in the matrix of \eq{coeff_system} when using either \eq{Fourier_Dn} ($k=\left\lfloor n/2 \right\rfloor$) or \eq{Chebyshev_Dn} ($k=n$). To obtain an invertible linear system, we next introduce the boundary conditions.

\subsubsection{Adding the linear system from the boundary conditions}

When we use the form \eq{u_expand} of $u(\bm{x})$ to write linear equations describing the boundary conditions \eq{proj_combination}, we obtain a non-sparse linear system. Thus, for each $\bm{x}\in \partial\D_j$ in \eq{proj_combination}, we perform the $(d-1)$-dimensional inverse QSFT/QCT on the $d-1$ registers except the $j$th register to obtain the linear equations
\begin{equation}
\begin{aligned}
\sum_{\substack{\|\bm{k}\|_{\infty}\le n \\ k_j=n}}c_{\bm{k}}|k_1\rangle\ldots|k_d\rangle&=\sum_{\substack{\|\bm{k}\|_{\infty}\le n \\ k_j=n}}\hat{\gamma}^{1+}_{\bm{k}}|k_1\rangle\ldots|k_d\rangle,\quad \hat{\gamma}^{j+}_{\bm{k}}\in\partial\D_j, \\
\sum_{\substack{\|\bm{k}\|_{\infty}\le n \\ k_j=n-1}}(-1)^{k_j}c_{\bm{k}}|k_1\rangle\ldots|k_d\rangle&=\sum_{\substack{\|\bm{k}\|_{\infty}\le n \\ k_j=n-1}}\hat{\gamma}^{1-}_{\bm{k}}|k_1\rangle\ldots|k_d\rangle,\quad \hat{\gamma}^{j-}_{\bm{k}}\in\partial\D_j
\label{eq:boundary_linear_system}
\end{aligned}
\end{equation}
for all $j \in [d]$, where the values of $k_j$ indicate that we place these constraints in the last two rows with respect to the $j$th coordinate.
We combine these equations with \eq{coeff_system} to obtain the linear system
\begin{equation}
\sum_{\|\bm{j}\|_1= 2}A_{\bm{j}}\sum_{\|\bm{k}\|_{\infty},\|\bm{r}\|_{\infty}\le n}[\overline{\bm{D}}^{(\bm{j})}_{n}]_{\bm{kr}}c_{\bm{r}}|k_1\rangle\ldots|k_d\rangle=\sum_{\|\bm{k}\|_{\infty}\le n}\sum_{j=1}^d (A_{j,j}\hat{\gamma}^{j+}_{\bm{k}}+A_{j,j}\hat{\gamma}^{j-}_{\bm{k}}+\hat{f}_{\bm{k}})|k_1\rangle\ldots|k_d\rangle,
\label{eq:coeff_linear_system}
\end{equation}
where
\begin{equation}
\overline{\bm{D}}^{(\bm{j})}_{n}=
\begin{cases}
\bm{D}^{(\bm{j})}_{n} + \bm{G}^{(\bm{j})}_{n}, \quad & \|\bm{j}\|_1= 2,~\|\bm{j}\|_{\infty}= 2;\\
\bm{D}^{(\bm{j})}_{n},  & \|\bm{j}\|_1= 2,~\|\bm{j}\|_{\infty}= 1
\end{cases}
\label{eq:boundary_tensor}
\end{equation}
with $\bm{G}^{(\bm{j})}_{n}$ defined below.
% \amc{say a few words about why we define it this way?}\jpl{Here we add the matrix $G_n$ into $\overline{\bm{D}}^{(\bm{j})}_{n}$ to guarantee $\overline{\bm{D}}^{(\bm{j})}_{n}$ is full rank when $\|\bm{j}\|_1= \|\bm{j}\|_{\infty}= 2$.}
In other words, $\overline{\bm{D}}^{(\bm{j})}_{n}=\bm{D}^{(\bm{j})}_{n}+\bm{G}^{(\bm{j})}_{n}$ for each $\bm{j}$ that has exactly one entry equal to $2$ and all other entries $0$, whereas $\overline{\bm{D}}^{(\bm{j})}_{n}=\bm{D}^{(\bm{j})}_{n}$ for each $\bm{j}$ that has exactly two entries equal to $1$ and all other entries $0$. Here $\bm{G}^{(\bm{j})}_{n}$ can be expressed as the tensor product
\begin{equation}
\bm{G}^{(\bm{j})}_{n} = I^{\otimes r-1}\otimes G_n\otimes I^{\otimes d-r}
\label{eq:diff_tensor_G}
\end{equation}
where the $r$th entry of $\bm{j}$ is 2 and all other entries are 0. For the Fourier case in \eq{basis_function} used for periodic boundary conditions, $D_n$ comes from \eq{Fourier_Dn}, and the nonzero entries of $G_n$ are
\begin{equation}
[G_n]_{\left\lfloor n/2 \right\rfloor,k}=1,\quad k \in\rangez{n+1}.
\label{eq:Fourier_Gn}
\end{equation}
Alternatively, for the Chebyshev case in \eq{basis_function} used for non-periodic boundary conditions, $D_n$ comes from \eq{Chebyshev_Dn}, and the nonzero entries of $G_n$ are
\begin{equation}
\begin{aligned}
&[G_n]_{n,k}=1,\quad &k \in\rangez{n+1},\\\
&[G_n]_{n-1,k}=(-1)^k,\quad &k \in\rangez{n+1}.
\label{eq:Chebyshev_Gn}
\end{aligned}
\end{equation}

The system \eq{coeff_linear_system} has the form of \eq{linear_system}. For instance, the matrix in \eq{linear_system} for Poisson's equation \eq{Poisson} is
\begin{align}
L_{\Poisson}&:=\overline{\bm D}_n^{(2,0,\ldots,0)}
+\overline{\bm D}_n^{(0,2,\ldots,0)}
+\cdots+\overline{\bm D}_n^{(0,0,\ldots,2)} \\
&= \bigoplus_{j=1}^d \overline{D}^{(2)}_n=\overline{D}^{(2)}_n\otimes I^{\otimes d-1}+I\otimes \overline{D}^{(2)}_n\otimes I^{\otimes d-2}+\cdots+I^{\otimes d-1}\otimes \overline{D}^{(2)}_n.
\label{eq:matrix_Poisson}
\end{align}
For periodic boundary conditions, using \eq{diff_transform}, \eq{Fourier_Dn}, and \eq{Fourier_Gn}, the second-order differential matrix $\overline{D}^{(2)}_n$ has nonzero entries
\begin{equation}
\begin{aligned}
[\overline{D}^{(2)}_n]_{k,k}&=-((k-\left\lfloor n/2 \right\rfloor)\pi)^2, & k&\in\rangez{n+1}\backslash\{\left\lfloor n/2 \right\rfloor\},\\
[\overline{D}^{(2)}_n]_{\left\lfloor n/2 \right\rfloor,k}&=1, & k&\in\rangez{n+1}.
\label{eq:matrix_Fourier}
\end{aligned}
\end{equation}
For non-periodic boundary conditions, using \eq{diff_transform}, \eq{Chebyshev_Dn}, and \eq{Chebyshev_Gn}, $\overline{D}^{(2)}_n$ has nonzero entries
\begin{equation}
\begin{aligned}
[\overline{D}^{(2)}_n]_{kr}&=\sum_{\substack{l=k+1 \\ \text{$k+l$ odd} \\ \text{$l+r$ odd}}}^{r-1}[D_n]_{kl}[D_n]_{lr}=\frac{2r}{\sigma_k}\sum_{\substack{l=k+1 \\ \text{$k+l$ odd} \\ \text{$l+r$ odd}}}^{r-1}\frac{2l}{\sigma_l}=\frac{r(r^2-k^2)}{\sigma_k}, &&\text{$k+r$ even},\, r>k+1,\\
[\overline{D}^{(2)}_n]_{n,k}&=1, & &k \in\rangez{n+1},\\
[\overline{D}^{(2)}_n]_{n-1,k}&=(-1)^k, & &k \in\rangez{n+1}.
\label{eq:matrix_Chebyshev}
\end{aligned}
\end{equation}

To explicitly illustrate this linear system, we present a simple example in \app{Poisson}. We discuss the invertible linear system \eq{coeff_linear_system} and upper bound its condition number in the following section.
