% !TEX root = ../multi-pde-draft.tex

\section{Introduction}
\label{sec:problem}

Many scientific problems involve partial differential equations (PDEs). Prominent examples include Maxwell's equations for electromagnetism, Boltzmann's equation and the Fokker-Planck equation in thermodynamics, and Schr{\"o}dinger's equation in continuum quantum mechanics. While models of physics are often studied in a constant number of spatial dimensions, it is also natural to study high-dimensional PDEs, such as to model systems with many interacting particles. Classical numerical methods have complexity that grows exponentially in the dimension, a phenomenon sometimes called the curse of dimensionality. This is a major challenge for attempts to solve PDEs on classical computers.

A common approach to solving PDEs on a digital computer is the finite difference method (FDM). In this approach, we discretize space into a rectangular lattice, solve a system of linear equations that approximates the PDE on the lattice, and output the solution on those grid points. If each spatial coordinate has $n$ discrete values, then $n^d$ points are needed to discretize a $d$-dimensional problem. Simply outputting the solution on these grid points takes time $\Omega(n^d)$.

Beyond uniform grids, the sparse grid technique \cite{Smo63} has been applied to reduce the time and space complexity of outputting a sparse encoding of the solution to $O(n\log^d n)$ \cite{BG04,Zen91}. While this is a significant improvement, it still scales exponentially in $d$. It can be shown that for a grid-based approach this complexity is optimal with respect to certain norms \cite{BG04}. Reference~\cite{BG04} proposes alternative sparse grid algorithms whose complexities scale linearly with $n$ but exponentially with $d$. Another grid-based method is the finite element method (FEM), where the differential equation is multiplied by functions with local support (restricted by the grid) and then integrated. This produces a set of equations that the solution must satisfy, which are then used to approximate the solution. In yet another grid-based approach, the finite volume method (FVM) considers a grid dividing space into volumes/cells. The field is integrated over these volumes to create auxiliary variables, and relations between these variables are derived from the differential equation.

An alternative to grid methods is the concept of \emph{spectral methods} \cite{Ghe07,STW11}. Spectral methods use linear combinations of basis functions (such as Fourier basis states or Chebyshev polynomials) to globally approximate the solution. These basis functions allow the construction of a linear system whose solution approximates the solution of the PDE.

These classical algorithms often consider the problem of outputting the solution at $N$ points in space, which clearly requires $\Omega(N)$ space and time. Quantum algorithms often (though not always) consider the alternative problem of outputting a quantum state proportional to such a vector, which requires only $\Omega(\log N)$ space---and correspondingly provides more limited access to the solution---but can potentially be done in only $\poly(\log N)$ time.

The fact that quantum states can efficiently encode exponentially long vectors has also been leveraged for the development of quantum linear system algorithms (QLSAs) \cite{HHL08,Amb12,CKS15}. For a linear system $A \vec x = \vec b$, a QLSA outputs a quantum state proportional to the solution $\vec x$. To learn information about the solution $\vec x$, the output of the QLSA must be post-processed. For example, to output all the entries of an $N$-dimensional vector $\vec x$ given a quantum state $\ket{x}$ proportional to it, even a quantum computer needs time and space $\Omega(N)$.

Because linear systems are often used in classical algorithms for PDEs such as those described above, it is natural to consider their quantum counterparts. Clader, Jacobs, and Sprouse~\cite{CJS13} give a heuristic algorithm for using sparse preconditioners and QLSAs to solve a linear system constructed using the FEM for Maxwell's equations. The state output by the QLSA is then post-processed to compute electromagnetic scattering cross-sections.

In subsequent work, Montanaro and Pallister~\cite{MP16} use QLSAs to implement the FEM for $d$-dimensional boundary value problems and evaluate the quantum speedup that can be achieved when estimating a function of the solution within precision $\epsilon$. This involves a careful analysis of how different algorithmic parameters (such as the dimension and condition number of the FEM linear system and the number of post-processing measurements) scale with respect to input variables (such as the spatial dimension $d$ and desired precision $\epsilon$), since all of these affect the complexity. Their algorithms have complexity $\poly(d, 1/\epsilon)$, compared to $O((1/\epsilon)^d)$ for the classical FEM. This exponential improvement with respect to $d$ suggests that quantum algorithms may be notably faster when $d$ is large. However, they also argue that for fixed $d$, at most a polynomial speed-up can be expected due to lower bounds on the cost of post-processing the state to estimate a function of the solution.

The FDM has also been used in quantum algorithms for PDEs. References~\cite{cao2013quantum,wang2019quantum} apply the FDM to solve Poisson's equation in rectangular volumes under Dirichlet boundary conditions. Although the circuits they construct have $\poly (\log (1/ \epsilon))$ gates, these circuits have success probability $\poly(1/\epsilon)$, leading to $\poly(1/\epsilon)$ time complexity. Additionally, they do not quantify errors resulting from the finite-difference approximation. Reference~\cite{CJO17} applies the FDM to the problem of outputting states proportional to solutions of the wave equation, giving complexity $d^{\frac{5}{2}}\poly(1/\epsilon)$, a polynomial dependence on $d$ and $1/\epsilon$ (which is $\poly(n)$ for a fixed-order FDM). The FVM is combined with the reservoir method in Ref.~\cite{Fillion-Gourdeau2018} to simulate hyperbolic equations; although they achieve linear scaling with respect to the spatial dimension, they use fixed order differences, leading to $\poly (1/ \epsilon )$ scaling. These FDM, FEM, and FVM approaches can only give a total complexity $\poly(1/\epsilon)$, even using high-precision methods for the QLSA or Hamiltonian simulation, because of the additional approximation errors in the FDM, FEM, and FVM.

The FDM is also applied in Ref.~\cite{kivlichan2017bounding} to simulate how a fixed number of particles evolve under the Schr\"{o}dinger equation with access to an oracle for the potential term. This can be seen as a special case of quantum algorithms for PDEs. Other examples include quantum algorithms for many-body quantum dynamics \cite{zalka1998efficient,wiesner1996simulations} and for electronic structure problems, including for quantum chemistry (see for example \cite{Reiher7555, lanyon2010towards}). However, here we focus on PDEs whose dynamics are not necessarily unitary.

In this paper, we propose new quantum algorithms for linear PDEs where the boundary is the unit hypercube. In the spirit of Ref.~\cite{MP16}, we state our results in terms of the approximation error and the spatial dimension; however, we do not consider the problem of estimating a function of the PDE solution and instead focus on outputting states encoding the solution, allowing us to give algorithms with complexity $\poly(\log(1/\epsilon))$. Just as for the QLSA, this improvement is potentially significant if the given equations must be solved as a subroutine within some larger computation. The problem we address can be informally stated as follows: Given a linear PDE with boundary conditions and an error parameter $\epsilon$, output a quantum state that is $\epsilon$-close to one whose amplitudes are proportional to the solution of the PDE at a set of grid points in the domain of the PDE.

Our first algorithm is based on a quantum version of the FDM approach: we use a finite-difference approximation to produce a system of linear equations and then solve that system using the QLSA. We analyze our FDM algorithm as applied to Poisson's equation under periodic, Dirichlet, and Neumann boundary conditions.
Whereas previous FDM approaches \cite{cao2013quantum,CJO17} considered fixed orders of truncation, we adjust the order of truncation depending on $\epsilon$.
As the order increases, the eigenvalues of the FDM matrix approach the eigenvalues of the continuous Laplacian, allowing for more precise approximations.
The main algorithm we present uses the quantum Fourier transform (QFT) and takes advantage of the high-precision LCU-based QLSA \cite{CKS15}. We first consider periodic boundary conditions, but by restricting to appropriate subspaces, this approach can also be applied to homogeneous Dirichlet and Neumann boundary conditions.

We also propose a quantum algorithm for more general second-order elliptic PDEs under periodic or non-periodic Dirichlet boundary conditions. This algorithm is based on quantum spectral methods \cite{CL19}. The spectral method globally approximates the solution of a PDE by a truncated Fourier or Chebyshev series (which converges exponentially for smooth functions) with undetermined coefficients, and then finds the coefficients by solving a linear system. This system is exponentially large in $d$, so solving it is infeasible for classical algorithms but feasible in a quantum context. To be able to apply the QLSA efficiently, we show how to make the system sparse using variants of the quantum Fourier transform.

Both of these approaches have complexity $\poly(d, \log(1/\epsilon))$, providing optimal dependence on $\epsilon$ and an exponential improvement over classical methods as a function of the spatial dimension $d$. Bounding the complexities of these algorithms requires analyzing how $d$ and $\epsilon$ affect the condition numbers of the relevant linear systems (finite difference matrices and matrices relating the spectral coefficients) and accounting for errors in the approximate solution provided by the QLSA.

\tab{alg-compare} compares the performance of our approaches to other classical and quantum algorithms for PDEs. Compared to classical algorithms, quantum algorithms improve the dependence on spatial dimension from exponential to polynomial (with the significant caveat that they produce a different representation of the solution). Compared to previous quantum FDM/FEM/FVM algorithms \cite{cao2013quantum,Fillion-Gourdeau2018,CJO17,MP16}, the quantum adaptive FDM and quantum spectral method improve the error dependence from $\poly(1/\epsilon)$ to $\poly(\log(1/\epsilon))$. Our approaches achieve the best known dependence on the parameters $d$ and $\epsilon$ for the Poisson equation with homogeneous boundary conditions. Furthermore, our quantum spectral method approach not only achieves the best known dependence on $d$ and $\epsilon$ for elliptic PDEs with inhomogeneous Dirichlet boundary conditions, but also improves the dependence on $d$ for the Poisson equation with inhomogeneous Dirichlet boundary conditions, as compared to previous quantum algorithms.

\begin{table}
\begin{center}
\footnotesize{
\renewcommand{\arraystretch}{1.25}
\begin{tabular}{|@{\hspace{1.5mm}}c|c|c|c|c|}
    \hline
     & \textbf{Algorithm} & \textbf{Equation} & \textbf{Boundary conditions}  & \textbf{Complexity} \\
    \hline
    \parbox[t]{1.5mm}{\multirow{5}{*}{\rotatebox[origin=c]{90}{Classical}}}
     & FDM/FEM/FVM  & general & general & $\poly((1/\epsilon)^d)$ \\
    \cline{2-5}
     & Adaptive FDM/FEM \cite{BG87}  & general & general & $\poly((\textcolor[rgb]{0.00,0.07,1.00}{\log(1/\epsilon)})^d)$ \\
    \cline{2-5}
     & Spectral method \cite{Ghe07,STW11} & general & general & $\poly((\textcolor[rgb]{0.00,0.07,1.00}{\log(1/\epsilon)})^d)$ \\
    \cline{2-5}
     & Sparse grid FDM/FEM \cite{BG04,Zen91}  & general & general & $\poly((1/\epsilon)(\log(1/\epsilon))^d)$ \\
    \cline{2-5}
     & Sparse grid spectral method \cite{SY10,SY12}  & elliptic & general & $\poly(\textcolor[rgb]{0.00,0.07,1.00}{\log(1/\epsilon)(\log\log(1/\epsilon))}^d)$ \\
    \hline
    \parbox[t]{1.5mm}{\multirow{7}{*}{\rotatebox[origin=c]{90}{Quantum}}}
     & FEM \cite{MP16}  & Poisson & homogeneous & $\poly(d, 1/\epsilon)$ \\
    \cline{2-5}
     & FDM \cite{cao2013quantum}  & Poisson & homogeneous Dirichlet & $\textcolor[rgb]{0.00,0.07,1.00}{d} \poly( \textcolor[rgb]{0.00,0.07,1.00}{\log d}, 1/\epsilon)$ \\
    \cline{2-5}
     & FDM \cite{CJO17}  & wave & homogeneous & $\textcolor[rgb]{0.00,0.07,1.00}{d^{5/2}} \poly(1/\epsilon)$ \\
    \cline{2-5}
     & FVM \cite{Fillion-Gourdeau2018}  & hyperbolic & periodic & $\textcolor[rgb]{0.00,0.07,1.00} d \poly(1/\epsilon)$ \\
    \cline{2-5}
     & Adaptive FDM [this paper]  & Poisson & periodic, homogeneous & $\textcolor[rgb]{0.00,0.07,1.00}{d^2} \poly( \textcolor[rgb]{0.00,0.07,1.00}{\log d}, \textcolor[rgb]{0.00,0.07,1.00}{\log(1/\epsilon)})$  \\
    \cline{2-5}
     & Spectral method [this paper]  & Poisson & homogeneous Dirichlet & $\textcolor[rgb]{0.00,0.07,1.00}{d} \poly(\textcolor[rgb]{0.00,0.07,1.00}{\log d}, \textcolor[rgb]{0.00,0.07,1.00}{\log(1/\epsilon)})$ \\
    \cline{2-5}
     & Spectral method [this paper]  & elliptic & inhomogeneous Dirichlet & $\textcolor[rgb]{0.00,0.07,1.00}{d^2} \poly( \textcolor[rgb]{0.00,0.07,1.00}{\log(1/\epsilon)})$ \\
    \hline
\end{tabular}
}
\end{center}
\caption{
Summary of the time complexities of classical and quantum algorithms for $d$-dimensional PDEs with error tolerance $\epsilon$. Portions of the complexity in blue represent best known dependence on that parameter.
\label{tab:alg-compare}
}
\end{table}

The remainder of the paper is structured as follows. \sec{pde-setup} introduces technical details about linear PDEs and formally states the problem we solve. \sec{fdm} covers our FDM algorithm for Poisson's equation. \sec{spectral} details the spectral algorithm for elliptic PDEs. Finally, \sec{discussion} concludes with a brief discussion of the results and some open problems.

\section{Linear PDEs}
\label{sec:pde-setup}

In this paper, we focus on systems of linear PDEs. Such equations can be written in the form
\begin{equation}
\L(u(\bm{x}))=f(\bm{x}),
\label{eq:pde}
\end{equation}
where the variable $\bm{x}=(x_1,\ldots,x_d) \in \C^d$ is a $d$-dimensional vector, the solution $u(\bm{x})\in\C$ and the inhomogeneity $f(\bm{x})\in\C$ are scalar functions, and $\L$ is a linear differential operator acting on $u(\bm{x})$. In general, $\L$ can be written in a linear combination of $u(\bm{x})$ and its derivatives. A linear differential operator $\L$ of order $h$ has the form
\begin{equation}
\L(u(\bm{x}))=\sum_{\|\bm{j}\|_1\le h}A_{\bm{j}}(\bm{x})\frac{\partial^{\bm{j}}}{\partial \bm{x}^{\bm{j}}}u(\bm{x}),
\label{eq:operator}
\end{equation}
where $\bm{j}=(j_1,\ldots,j_d)$ is a $d$-dimensional non-negative vector with $\norm{\bm{j}}_1 = j_1+\dots+j_d\le h $, $A_j(\bm{x}) \in \C$, and
\begin{equation}
\frac{\partial^{\bm{j}}}{\partial \bm{x}^{\bm{j}}}u(\bm{x})=\frac{\partial^{j_1}}{\partial x_1^{j_1}}\cdots\frac{\partial^{j_d}}{\partial x_d^{j_d}}u(\bm{x}).
\label{eq:derivative}
\end{equation}
The problem reduces to a system of linear ordinary differential equations (ODEs) when $d=1$. For $d\ge2$, we call \eq{pde} a (multi-dimensional) PDE.

For example, systems of first-order linear PDEs can be written in the form
\begin{equation}
\sum_{j=1}^dA_j(\bm{x})\frac{\partial u(\bm{x})}{\partial x_j}+A_0(\bm{x})u(\bm{x})=f(\bm{x}),
\label{eq:fpde}
\end{equation}
where
% $\L$ is explicitly written as a linear combination of $A_J(\bm{x})\frac{\partial}{\partial x_J}$ and $A(\bm{x})$, with
$A_j(\bm{x}), A_0(\bm{x}), f(\bm{x}) \in\C$ for $j \in \range{d} := \{1,\ldots,d\}$.
Similarly, systems of second-order linear PDEs can be expressed in the form
\begin{equation}
\sum_{j_1,j_2=1}^dA_{j_1j_2}(\bm{x})\frac{\partial^2 u(\bm{x})}{\partial x_{j_1} \partial x_{j_2}}+\sum_{j=1}^dA_j(\bm{x})\frac{\partial u(\bm{x})}{\partial x_j}+A_0(\bm{x})u(\bm{x})=f(\bm{x}),
\label{eq:spde}
\end{equation}
where $A_{j_1,j_2}(\bm{x}), A_j(\bm{x}), A_0(\bm{x}), f(\bm{x}) \in\C$ for $j_1,j_2,j \in \range{d}$. A well-known second-order linear PDEs is the \emph{Poisson equation}
\begin{equation}
\Delta u(\bm{x}):=\sum_{j=1}^d\frac{\partial^2}{\partial x_j^2}u(\bm{x})=f(\bm{x}).
\label{eq:Poisson}
\end{equation}

A linear PDE of order $h$ is called \emph{elliptic} if its differential operator \eq{operator} satisfies
\begin{equation}
\sum_{\|\bm{j}\|_1= h}A_{\bm{j}}(\bm{x})\bm{\xi}^{\bm{j}}\ne0,
\label{eq:elliptic}
\end{equation}
for all nonzero $\bm{\xi}^{\bm{j}}=\xi_1^{j_1}\ldots\xi_d^{j_d}$ with $\xi_1,\ldots,\xi_d\in\R^m$.
% For even $h$, \eq{operator} is called \emph{uniformly elliptic} or \emph{strongly elliptic} if
% \begin{equation}
% \biggl|\sum_{\|\bm{j}\|_1= h}A_{\bm{j}}(\bm{x})\bm{\xi}^{\bm{j}}\biggr|\ge \overline{C}\|\xi\|^h,
% \label{eq:uniform}
% \end{equation}
% where $\xi=(\xi_1,\ldots,\xi_d)$, and $\overline{C}$ is a positive constant.
Note that ellipticity only depends on the highest-order terms.
When $h=2$, the linear PDE \eq{spde} is called a second-order elliptic PDE if and only if $A_{j_1j_2}(\bm{x})$ is positive-definite or negative-definite for any $\bm{x}$. In particular, the Poisson equation \eq{Poisson} is a second-order elliptic PDE.
% Equation \eq{spde} is uniformly elliptic if
% \begin{equation}
% \biggl\|\sum_{j_1,j_2=1}^dA_{j_1j_2}(\bm{x})\xi_{j_1}\xi_{j_2}\biggr\|\ge \overline{C}\|\xi\|^2,
% \label{eq:uniform_second}
% \end{equation}
% which is equivalent to
% \begin{equation}
% \min_{\|\xi\|_2=1}\biggl|\sum_{j_1,j_2=1}^dA_{j_1j_2}(\bm{x})\xi_{j_1}\xi_{j_2}\biggr|\ge \overline{C}.
% \label{eq:uniform_second_order}
% \end{equation}

We consider a class of %uniformly
elliptic PDEs that also satisfy the following condition:
\diff{
\begin{equation}
C := 1-\sum_{j_1=1}^d\frac{1}{|A_{j_1,j_1}(\bm{x})|}\sum_{j_2\in\range{d}\backslash\{j_1\}} |A_{j_1,j_2}(\bm{x})| > 0.
%C := \sum_{j=1}^d |A_{j,j}(\bm{x})|-\sum_{j_1\ne j_2}|A_{j_1,j_2}(\bm{x})| > 0.
\label{eq:DDM}
\end{equation}
}
We call this condition \emph{\gdd}, since it is a \diff{strengthening} of standard (strict) diagonal dominance. Observe that \eq{DDM} holds for the Poisson equation \eq{Poisson} with \diff{$C=1$}.
%Also note that if $A$ were strictly diagonally dominant with all diagonal entries having the same sign, this would imply \eq{elliptic} and \eq{DDM}.
% Furthermore, since uniform ellipticity is also a relaxation of a Hermitian strict diagonal dominance with all real positive(negative) diagonal entries, we consider \eq{uniform_second_order} joint with \eq{DDM} as a weaker assumption for PDEs instead of strict diagonal dominance.

%Furthermore, it strengthens the uniform ellipticity condition, since \eq{uniform_second_order} implies
%\begin{align}
%2|A_{j_1,j_2}(\bm{x})|
%&< 2\sqrt{|A_{j_1,j_1}(\bm{x})||A_{j_2,j_2}(\bm{x})|}
%\le |A_{j_1,j_1}(\bm{x})| + |A_{j_2,j_2}(\bm{x})|
%\end{align}
%for all $j_1,j_2$, which yields
%\begin{align}
%  (d-1)\sum_{j=1}^d |A_{j,j}(\bm{x})| - \sum_{j_1\ne j_2}|A_{j_1,j_2}(\bm{x})| > 0.
%\end{align}

%\amc{This does not show that \eq{DDM} implies \eq{uniform_second_order}, even with the additional condition that the entries all have the same sign. Is that even true? Is positivity of $2 \times 2$ principal submatrices enough to imply global positivity? Also consider relationship to standard diagonal dominance. Sort this out and then update the conditions accordingly.}

In this paper, we focus on the following boundary value problem:

\begin{problem}\label{prb:pde}
In the \emph{quantum PDE problem}, we are given a system of second-order elliptic equations
\begin{equation}
\L(u(\bm{x}))=\sum_{\|\bm{j}\|_1=2}A_{\bm{j}}\frac{\partial^{\bm{j}}}{\partial \bm{x}^{\bm{j}}}u(\bm{x})=\sum_{j_1,j_2=1}^dA_{j_1j_2}\frac{\partial^2 u(\bm{x})}{\partial x_{j_1} \partial x_{j_2}}=f(\bm{x})
\end{equation}
satisfying
% the conditions of uniform ellipticity \eq{uniform_second_order} and
the \gdd\ condition \eq{DDM},
where the variable $\bm{x}=(x_1,\ldots,x_d) \in \D=[-1, 1]^d$ is a $d$-dimensional vector, the inhomogeneity $f(\bm{x})\in\C$ is a scalar function of $\bm{x}$ satisfying $f(\bm{x}) \in C^\infty$, and the linear coefficients $A_{\bm{j}}\in\C$. We are also given boundary conditions $u(\bm{x}) = \gamma(\bm{x}) \in \partial\D$ or $\frac{\partial u(\bm{x})}{\partial x_j} {\big |}_{x_j= \pm 1} = \gamma (\bm{x}) |_{x_j = \pm 1} \in \partial\D$ where $\gamma (\bm{x}) \in C^{\infty} $. We assume there exists a weak solution $\hat{u}(\bm{x})\in\C$ for the boundary value problem (see Ref.~\cite[Section 6.1.2]{eve10}). Given oracles that compute the coefficients $A_{\bm{j}}$, and that prepare normalized states $|\gamma(\bm{x})\rangle$  and $|f(\bm{x})\rangle$ whose amplitudes are proportional to $\gamma(\bm{x})$ and $f(\bm{x})$ on a set of interpolation nodes $\bm{x}$, the goal is to output a quantum state $|u(\bm{x})\rangle$ whose amplitudes are proportional to $u(\bm{x})$ on a set of interpolation nodes $\bm{x}$.
\end{problem}
