\section{Partial Differential Equations Problem}
\label{sec:problem}

In this paper, we focus on systems of linear partial differential equations (PDEs). Such equations can be written in the form
\begin{equation}
\L(u(x))=f(x),
\label{eq:pde}
\end{equation}
where the variable $x$ is a $d$-dimensional vector, the solution $u(x)\in\C^m$ and inhomogeneity $f(x)\in\C^m$ are $m$-dimensional vectors, and $\L$ is a linear operator acting on $u(x)$. In general, $\L$ can be written in a linear combination of $u(x)$, $\frac{\partial u(x)}{\partial x_i}$, $\frac{\partial^2 u(x)}{\partial x_i \partial x_j}$, and possible higher order derivatives of $u(x)$. The problem is degenerated to systems of linear ordinary different equations (ODE) when $d=1$. We call this problem \eq{pde} multidimensional PDE when $d>2$, and general $d$ can be extremely large for many problems.

For example, systems of first-order linear partial differential equations can be written in the form
\begin{equation}
\sum_{i=1}^dA_i(x)\frac{\partial u(x)}{\partial x_i}+B(x)u(x)=f(x),
\label{eq:fpde}
\end{equation}
where $\L$ is explicitly written as a linear combination of $A_i(x)\frac{\partial}{\partial x_i}$ and $B(x)$, with $A_i(x), B(x) \in\C^{m\times m}$.

One of the famous problems for multi-dimensional PDEs is Poisson equation
\begin{equation}
\Delta u(x)=\sum_{i=1}^d\frac{\partial^2 u(x)}{\partial x_i^2}=f(x).
\label{eq:Poisson}
\end{equation}

Many scientific and engineering problems are required to solve multi-dimensional PDEs. For high dimensional problems, classical numerical methods suffer the exponentially growth of the complexity dependent on $d$, which is so-called Curse of Dimensionality, and thus numerical methods become non-feasible quickly.

In this paper, we propose a new quantum algorithm for solving general linear PDE problems. Our algorithm is based on quantum spectral methods and multi-dimensional Quantum Fourier Transform (QFT) or Quantum Cosine Transform (QCT). Using this approach, we give a quantum algorithm with complexity $\poly(d, \log m, \log(1/\epsilon))$, which matches the optimal result for $\epsilon$, $m$ and has exponential improvement over spacial dimension $d$.


