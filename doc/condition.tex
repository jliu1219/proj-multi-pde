% !TEX root = ../multi-pde-draft.tex

\subsection{Condition number}
\label{sec:condition}

We now analyze the condition number of the linear system. We begin with two lemmas bounding the singular values of the matrices \eq{matrix_Fourier} and \eq{matrix_Chebyshev} that appear in the linear system.

\begin{restatable}{lemma}{svdFourier}\label{lem:svd_Fourier}
Consider the case of periodic boundary conditions. Then the largest and smallest singular values of $\overline{D}^{(2)}_n$ defined in \eq{matrix_Fourier} satisfy
\begin{equation}
\begin{aligned}
&\sigma_{\max}(\overline{D}^{(2)}_n) \le (2n)^{2.5},\\
&\sigma_{\min}(\overline{D}^{(2)}_n) \ge \frac{1}{\sqrt{2}}.
\label{eq:svd_Fourier}
\end{aligned}
\end{equation}
\end{restatable}

\begin{restatable}{lemma}{svdChebyshev}\label{lem:svd_Chebyshev}
Consider the case of non-periodic boundary conditions. Then the largest and smallest singular values of $\overline{D}^{(2)}_n$ defined in \eq{matrix_Chebyshev} satisfy
\begin{equation}
\begin{aligned}
&\sigma_{\max}(\overline{D}^{(2)}_n) \le n^4,\\
&\sigma_{\min}(\overline{D}^{(2)}_n) \ge \frac{1}{16}.
\label{eq:svd_Chebyshev}
\end{aligned}
\end{equation}
\end{restatable}

The proofs of \lem{svd_Fourier} and \lem{svd_Chebyshev} appear in \app{svd}. Using these two lemmas, we first upper bound the condition number of the linear system for Poisson's equation, and then extend the result to general elliptic PDEs.

For the case of the Poisson equation, we use the following simple bounds on the extreme singular values of a Kronecker sum.

\begin{lemma}\label{lem:condition_Poisson}
Let
\begin{equation}
L=\bigoplus_{j=1}^d M_j=M_1\otimes I^{\otimes d-1}+I\otimes M_2\otimes I^{\otimes d-2}+\cdots+I^{\otimes d-1}\otimes M_d,
\label{eq:tensorsum}
\end{equation}
where $\{M_j\}_{j=1}^d$ are square matrices. If the largest and smallest singular values of $M_j$ satisfy
\begin{equation}
\begin{aligned}
&\sigma_{\max}(M_j) \le s_j^{\max},\\
&\sigma_{\min}(M_j) \ge s_j^{\min},
\label{eq:svd_tensorsum}
\end{aligned}
\end{equation}
respectively, then the condition number of $L$ satisfies
\begin{equation}
\kappa_L\le \frac{\sum_{j=1}^ds_j^{\max}}{\sum_{j=1}^ds_j^{\min}}.
\label{eq:con_tensorsum}
\end{equation}
\end{lemma}

\begin{proof}
We bound the singular values of the matrix exponential $\exp(M_j)$ by
\begin{equation}
\begin{aligned}
&\sigma_{\max}(\exp(M_j)) \le e^{s_j^{\max}},\\
&\sigma_{\min}(\exp(M_j)) \ge e^{s_j^{\min}}
\label{eq:svd_exp}
\end{aligned}
\end{equation}
using \eq{svd_tensorsum}. The singular values of the Kronecker product $\bigotimes_{j=1}^d\exp(M_j)$ are
\begin{equation}
\sigma_{k_1,\ldots,k_d}\biggl(\bigotimes_{j=1}^d\exp(M_j)\biggr)=\prod_{j=1}^d\sigma_{k_j}(\exp(M_j))
% ,\quad k_1,\ldots,k_d \in \rangez{n+1},
\end{equation}
where $\sigma_{k_j}(\exp(M_j))$ are the singular values of the matrix $\exp(M_j)$ for each $j \in \range{d}$, where $k_j$ runs from $1$ to the dimension of $M_j$. Using the property of the Kronecker sum that
\begin{equation}
\exp(L)=\exp\biggl(\bigoplus_{j=1}^d M_j\biggr)=\bigotimes_{j=1}^d\exp(M_j),
\label{eq:Kronecker}
\end{equation}
we bound the singular values of the matrix exponential of \eq{matrix_Poisson} by
\begin{equation}
\begin{aligned}
&\sigma_{\max}(\exp(L)) \le e^{\sum_{j=1}^ds_j^{\max}},\\
&\sigma_{\min}(\exp(L)) \ge e^{\sum_{j=1}^ds_j^{\min}}.
\label{eq:svd_prod}
\end{aligned}
\end{equation}
Finally, we bound the singular values of the matrix logarithm of \eq{svd_prod} by
\begin{equation}
\begin{aligned}
&\sigma_{\max}(L) \le \sum_{j=1}^ds_j^{\max},\\
&\sigma_{\min}(L) \ge \sum_{j=1}^ds_j^{\min}.
\label{eq:svd_Poisson}
\end{aligned}
\end{equation}
Thus the condition number of $L$ satisfies
\begin{equation}
\kappa_L \le \frac{\sum_{j=1}^ds_j^{\max}}{\sum_{j=1}^ds_j^{\min}}
\end{equation}
as claimed.
\end{proof}

This lemma easily implies a bound on the condition number of the linear system for Poisson's equation:

\begin{corollary}\label{cor:condition_Poisson}
Consider an instance of the quantum PDE problem as defined in \prb{pde} for Poisson's equation \eq{Poisson} with Dirichlet boundary conditions \eq{dbc}. The condition number of $L_{\Poisson}$ in the linear system \eq{linear_system} satisfies
\begin{equation}
\kappa_{L_{\Poisson}}\le (2n)^4.
\end{equation}
\end{corollary}

\begin{proof}
The matrix in \eq{linear_system} for Poisson's equation \eq{Poisson} is $L_{\Poisson}$ defined in \eq{matrix_Poisson}.
% with $\overline{D}^{(2)}_n$ defined by \eq{matrix_Fourier} for periodic boundary conditions or \eq{matrix_Chebyshev} for non-periodic boundary conditions.
For both the periodic and the non-periodic case, we have
\begin{equation}
\begin{aligned}
&\sigma_{\max}(\overline{D}^{(2)}_n) \le n^4,\\
&\sigma_{\min}(\overline{D}^{(2)}_n) \ge \frac{1}{16}
\label{eq:svd_Fourier_Chebyshev}
\end{aligned}
\end{equation}
by \lem{svd_Fourier} and \lem{svd_Chebyshev}. Let $M_j=\overline{D}^{(2)}_n$ for $j\in\range{d}$ in \eq{tensorsum}, and apply \lem{condition_Poisson} with $s_j^{\max}=n^4$ and $s_j^{\min}={1}/{16}$ in \eq{con_tensorsum}. Then the condition number of $L_{\Poisson}$ is bounded by
\begin{equation}
\kappa_{L_{\Poisson}} \le \frac{\sigma_{\max}(\overline{D}^{(2)}_n)}{\sigma_{\min}(\overline{D}^{(2)}_n)} \le (2n)^4
\end{equation}
as claimed.
\end{proof}

We now consider the condition number of the linear system for general elliptic PDEs.

\begin{lemma}\label{lem:condition}
Consider an instance of the quantum PDE problem as defined in \prb{pde} with Dirichlet boundary conditions \eq{dbc}.
%If
%\begin{equation}
%C:=\sum_{j=1}^d |A_{j,j}|-\sum_{j_1\ne j_2}|A_{j_1,j_2}|>0,
%\label{eq:DDM}
%\end{equation}
The condition number of $L$ in the linear system \eq{linear_system} satisfies
\begin{equation}
\kappa_L\le \frac{\|A\|_\Sigma}{C\|A\|_{\ast}}(2n)^4,
\end{equation}
where $\|A\|_{\Sigma}:=\sum_{\|\bm{j}\|_1\le 2} |A_{\bm{j}}|=\sum_{j_1,j_2=1}^d |A_{j_1,j_2}|$, \diff{$\|A\|_{\ast}:=\sum_{j=1}^d |A_{j,j}|$, }and the constant $C$ comes from \eq{DDM}.
\end{lemma}

%We call condition \eq{DDM} \emph{global strict diagonal dominance}, since it is a strengthening of standard strict diagonal dominance. Also observe that \eq{DDM} strengthens the uniform ellipticity condition \eq{uniform}, since \eq{uniform} implies $2|A_{j_1,j_2}| < 2\sqrt{|A_{j_1,j_1}||A_{j_2,j_2}|} \le |A_{j_1,j_1}| + |A_{j_2,j_2}|$, which yields $(d-1)\sum_{j=1}^d |A_{j,j}|-\sum_{j_1\ne j_2}|A_{j_1,j_2}|>0$.
% \amc{if we need to include this condition, can we argue that it often holds?}\jpl{It holds for the Poisson equation, but not for all elliptic PDEs. We can say it holds for diagonal dominant matrices.}\amc{We should certainly mention that it holds for the Poisson case. I understand that it doesn't hold generally. But can we argue that it's likely to hold in realistic applications?}\jpl{I moved the introduction of global strict diagonal dominance right before Problem 1.}

\begin{proof}
According to \eq{coeff_linear_system}, the matrix in \eq{linear_system} is
\begin{equation}
L=\sum_{\|\bm{j}\|_1= 2}A_{\bm{j}}\overline{\bm{D}}^{(\bm{j})}_{\bm{n}}.
\label{eq:matrix_L}
\end{equation}
We upper bound the spectral norm of the matrix $L$ by
\begin{equation}
\|L\| \le \sum_{\|\bm{j}\|_1= 2} |A_{\bm{j}}| \|\overline{\bm{D}}^{(\bm{j})}_{\bm{n}}\|.
\end{equation}
For the matrix $\overline{\bm{D}}^{(\bm{j})}_{\bm{n}}$ defined by \eq{boundary_tensor}, \lem{svd_Fourier} (in the periodic case) and \lem{svd_Chebyshev} (in the non-periodic case) give the inequality
\begin{equation}
\|\overline{\bm{D}}^{(\bm{j})}_{\bm{n}}\|\le n^4,
\label{eq:bound_Dn}
\end{equation}
so we have
\begin{equation}
\|L\| \le \sum_{\|\bm{j}\|_1= 2} |A_{\bm{j}}| n^4 = \|A\|_\Sigma \, n^4.
\label{eq:matrix_norm}
\end{equation}

Next we lower bound $\|L\xi\|$ for any $\|\xi\|=1$.

%The eigenvalues of $L$ are
%\begin{equation}
%\sum_{\|\bm{j}\|_1= 2}A_{\bm{j}}\bm{\lambda}^{\bm{j}}_{\bm{k}},
%\label{eq:chara_poly}
%\end{equation}
%with
%\begin{equation}
%\bm{\lambda}^{\bm{j}}_{\bm{k}}=\lambda_{k_1}^{j_1}\lambda_{k_2}^{j_2}\ldots\lambda_{k_d}^{j_d},
%\end{equation}
%where $\lambda_{k_j}$ are eigenvalues of the matrix $\overline{D}^{(2)}_n$ for each $k_j \in \rangez{n+1}$. Since \eq{chara_poly} does not have real roots when viewed as a polynomial in $\lambda_{k_1},\ldots,\lambda_{k_d}$ (by the uniform ellipticity condition \eq{uniform_second_order}), none of the eigenvalues \eq{chara_poly} are equal to zero. This shows that the linear system \eq{coeff_linear_system} is invertible.

It is non-trivial to directly compute the singular values of a sum of non-normal matrices. Instead, we write $L$ as a sum of terms $L_1$ and $L_2$, where $L_1$ is a tensor sum similar to \eq{matrix_Poisson} that can be bounded by \lem{condition_Poisson}, and $L_2$ is a sum of tensor products that are easily bounded. Specifically, we have
\begin{equation}
\begin{aligned}
L_1&=A_{1,1}\overline{D}^{(2)}_n\otimes I^{\otimes d-1}+\cdots+A_{d,d}I^{\otimes d-1}\otimes \overline{D}^{(2)}_n\\
L_2&=L-L_1.
\end{aligned}
\end{equation}
The ellipticity condition \eq{elliptic} can only hold if the $A_{j,j}$ for $j \in \range{d}$ are either all positive or all negative; we consider $A_{j,j}>0$ without loss of generality\diff{, so}
\begin{equation}
\|A\|_{\ast}\diff{=}\sum_{j=1}^d |A_{j,j}|=\sum_{j=1}^dA_{j,j}.
\end{equation}
then \eq{DDM} can be rewritten as
\diff{
\begin{equation}
1-\sum_{j_1=1}^d\frac{1}{A_{j_1,j_1}}\sum_{j_2\in\range{d}\backslash\{j_1\}} |A_{j_1,j_2}|=C>0,
%\|A\|_{\ast}-\sum_{j_1\ne j_2}|A_{j_1,j_2}|=C>0,
\label{eq:discrete_uniform}
\end{equation}
where $0 < C \le 1$.
}

We now upper bound $\|L_2 L_1^{-1}\|$ by bounding $\|\bm{D}^{(\bm{j})}_{\bm{n}} L_1^{-1}\|$ for each $\bm{j}=(j_1, \ldots, j_d)$ that has exactly two entries equal to $1$ and all other entries $0$. Specifically, consider $j_{r_1}=j_{r_2}=1$ for $r_1, r_2 \in \range{d}$, $r_1\ne r_2$, and $j_{r}=0$ for $r \in \range{d}\backslash\{r_1, r_2\}$. We denote
\begin{equation}
L^{(\bm{j})}:=I^{\otimes r_1-1}\otimes D^{2}_n\otimes I^{\otimes d-r_1}+I^{\otimes r_2-1}\otimes D^{2}_n\otimes I^{\otimes d-r_2}.
\end{equation}
We first upper bound $\|\bm{D}^{(\bm{j})}_{\bm{n}}\|$ by $\frac{1}{2}\|L^{(\bm{j})}\|$. Notice the matrices $\bm{D}^{(\bm{j})}_{\bm{n}}$ and $L^{(\bm{j})}$ share the same singular vectors. For $k \in \rangez{n+1}$, we let $v_k$ and $\lambda_k$ denote the right singular vectors and corresponding singular values of $D_n$, respectively. Then the right singular vectors of $\bm{D}^{(\bm{j})}_{\bm{n}}$ and $L^{(\bm{j})}$ are $\bm{v}_{\bm{k}}:=\bigotimes_{j=1}^d v_{k_j}$, where $\bm{k}=(k_1,\ldots,k_d)$ with $k_j \in \rangez{n+1}$ for $j \in \range{d}$. For any vector $v=\sum_{\|\bm{k}\|_\infty \le n}\alpha_{\bm{k}}\bm{v}_{\bm{k}}$, we have
\begin{equation}
\|\bm{D}^{(\bm{j})}_{\bm{n}}v\|^2 = \sum_{\|\bm{k}\|_\infty \le n}|\alpha_{\bm{k}}|^2\|\bm{D}^{(\bm{j})}_{\bm{n}}\bm{v}_{\bm{k}}\|^2 = \sum_{\|\bm{k}\|_\infty \le n}|\alpha_{\bm{k}}|^2(\lambda_{k_{j_{r_1}}}\lambda_{k_{j_{r_2}}})^2,
\label{eq:svd_Dj}
\end{equation}
\begin{equation}
\|L^{(\bm{j})}v\|^2 = \sum_{\|\bm{k}\|_\infty \le n}|\alpha_{\bm{k}}|^2\|L^{(\bm{j})}\bm{v}_{\bm{k}}\|^2 = \sum_{\|\bm{k}\|_\infty \le n}|\alpha_{\bm{k}}|^2(\lambda_{k_{j_{r_1}}}^2+\lambda_{k_{j_{r_2}}}^2)^2,
\label{eq:svd_Lj}
\end{equation}
which implies $\|\bm{D}^{(\bm{j})}_{\bm{n}}v\| \le \frac{1}{2}\|L^{(\bm{j})}v\|$ by the AM-GM inequality. Since this holds for any vector $v$, we have
\begin{equation}
\|\bm{D}^{(\bm{j})}_{\bm{n}}L_1^{-1}\| \le \frac{1}{2}\|L^{(\bm{j})}L_1^{-1}\|.
\end{equation}
% \jpl{I might prefer this version of proof instead of writing a separate lemma. This step is based on the tensor structure of our linear system, related to our parameters $n$, $d$, and $j_{r_1}, j_{r_2}$. I also feel comfortable for the proof flow, while to introduce a lemma in the middle of the proof is a little bit weird.} \amc{it could be a separate technical lemma handled before the start of this proof, but this was is okay tood}

Next we upper bound $\|D^{2}_n\|$ by $\|\overline{D}^{(2)}_n\|$. For any vector $u=[u_0,\ldots,u_n]^T$, define two vectors $w=[w_0,\ldots,w_n]^T$ and $\overline{w}=[\overline{w}_0,\ldots,\overline{w}_n]^T$ such that
\begin{equation}
D^{2}_n[u_0,\ldots,u_n]^T = [w_0,\ldots,w_n]^T
\end{equation}
and
\begin{equation}
\overline{D}^{2}_n[u_0,\ldots,u_n]^T = [\overline{w}_0,\ldots,\overline{w}_n]^T.
\end{equation}
Notice that $w_{\left\lfloor n/2 \right\rfloor}=0$ and $w_k=\overline{w}_k$ for $k \in \rangez{n+1}\backslash\{\left\lfloor n/2 \right\rfloor\}$ for periodic conditions, and $w_{n-1}=w_n=0$ and $w_k=\overline{w}_k$ for $k \in \rangez{n+1}\backslash\{n-1,n\}$ for non-periodic conditions. Thus, for any vector $v$,
\begin{equation}
\|D^{2}_nv\|^2 =\|w\|^2 = \sum_{k=0}^nw_k^2 \le \sum_{k=0}^n\overline{w}_k^2 = \|\overline{w}\|^2 = \|\overline{D}^{(2)}_nv\|^2.
\end{equation}
Therefore,
\begin{equation}
\|L^{(\bm{j})}L_1^{-1}\| \le \sum_{s=1}^2\|I^{\otimes r_s-1}\otimes D^{2}_n\otimes I^{\otimes d-r_s}L_1^{-1}\| \le \sum_{s=1}^2\|I^{\otimes r_s-1}\otimes \overline{D}^{(2)}_n\otimes I^{\otimes d-r_s}L_1^{-1}\|.
\end{equation}
We also have
\begin{equation}
\|\bm{D}^{(\bm{j})}_{\bm{n}} L_1^{-1}\| \le \frac{1}{2}\sum_{s=1}^2\|I^{\otimes r_s-1}\otimes \overline{D}^{(2)}_n\otimes I^{\otimes d-r_s}L_1^{-1}\|.
\end{equation}
We can rewrite $I^{\otimes r_s-1}\otimes \overline{D}^{(2)}_n\otimes I^{\otimes d-r_s}L_1^{-1}$ in the form
\begin{equation}
I^{\otimes r_s-1}\otimes \overline{D}^{(2)}_n\otimes I^{\otimes d-r_s}\left(\sum_{h=1}^dA_{h,h}I^{\otimes r_h-1}\otimes \overline{D}^{(2)}_n\otimes I^{\otimes d-r_h}\right)^{-1}.
\end{equation}
The matrices $I^{\otimes r_h-1}\otimes \overline{D}^{(2)}_n\otimes I^{\otimes d-r_h}$ share the same singular values and singular vectors, so
\begin{align}
\|I^{\otimes r_s-1}\otimes \overline{D}^{(2)}_n\otimes I^{\otimes d-r_s}L_1^{-1}\| =  \max_{\overline{\lambda}_{k_r}}\frac{\overline{\lambda}_{k_{r_s}}}{\sum_{h=1}^d A_{h,h}\overline{\lambda}_{k_h}} < \frac{1}{A_{{r_s},{r_s}}},
%\frac{1}{\sum_{j=1}^d A_{j,j}} = \frac{1}{\|A\|_{\ast}},
\end{align}
where $\overline{\lambda}_{k_h}$ are singular values of $I^{\otimes r_h-1}\otimes \overline{D}^{(2)}_n\otimes I^{\otimes d-r_h}$ for $k_h \in \rangez{n}$, $h\in \range{d}$. This implies
\begin{equation}
\|\bm{D}^{(\bm{j})}_{\bm{n}} L_1^{-1}\| \le \frac{1}{2}(\frac{1}{A_{{r_1},{r_1}}}+\frac{1}{A_{{r_2},{r_2}}}).
%\|\bm{D}^{(\bm{j})}_{\bm{n}} L_1^{-1}\| \le \frac{1}{\|A\|_{\ast}}.
\end{equation}
Using \eq{discrete_uniform}, considering each instance of $\bm{D}^{(\bm{j})}_{\bm{n}}$ in $L_2$, we have \diff{
\begin{equation}
\|L_2L_1^{-1}\| \le \sum_{j_1\ne j_2}|A_{j_1,j_2}| \|\bm{D}^{(\bm{j})}_{\bm{n}} L_1^{-1}\| \le \sum_{j_1=1}^d\frac{1}{A_{j_1,j_1}}\sum_{j_2\in\range{d}\backslash\{j_1\}} |A_{j_1,j_2}| \le 1-C.
%\frac{1}{\|A\|_{\ast}}\sum_{j_1\ne j_2} |A_{j_1,j_2}| = 1-\frac{C}{\|A\|_{\ast}}.
\end{equation}
}
%\jpl{where we need an assumption
%\begin{equation}
%1-\sum_{j_1=1}^d\frac{\sum_{j_1\ne j_2} |A_{j_1,j_2}|}{A_{j_1,j_1}} \ge C^{\ast}>0.
%\end{equation}
%}
Since $L$ and $L_1$ are invertible, $\|L_1^{-1}L_2\|\le 1-C<1$, and by \lem{condition_Poisson} applied to $\|L_1^{-1}\|$, we have
\begin{equation}
\|L^{-1}\|=\|(L_1+L_2)^{-1}\| \le \|(I+L_2L_1^{-1})^{-1}\| \|L_1^{-1}\| \le \frac{\|L_1^{-1}\|}{1-\|L_2L_1^{-1}\|}\le\frac{1/\frac{1}{16}\|A\|_{\ast}}{C}=\frac{16}{C\|A\|_{\ast}}.
%\frac{1/\frac{1}{16}\|A\|_{\ast}}{C/\|A\|_{\ast}}=\frac{16}{C}.
\end{equation}
Thus we have
\begin{equation}
\kappa_L = \|L\|\|L^{-1}\| \le \frac{\|A\|_\Sigma}{C\|A\|_{\ast}}(2n)^4
%\frac{\|A\|_\Sigma}{C}(2n)^4
\end{equation}
as claimed.
\end{proof}
