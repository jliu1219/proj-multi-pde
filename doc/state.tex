% !TEX root = ../multi-pde-draft.tex

\subsection{State preparation}
\label{sec:state}

We now describe a state preparation procedure for the vector $\vec f + \vec g$ in the linear system \eq{linear_system}.

\begin{lemma}\label{lem:preparation}
Let $O_f$ be a unitary oracle that maps $|0\rangle|0\rangle$ to a state proportional to $|0\rangle|f\rangle$, and $|\phi\rangle|0\rangle$ to $|\phi\rangle|0\rangle$ for any $|\phi\rangle$ orthogonal to $|0\rangle$; let $O_x$ be a unitary oracle that maps $|0\rangle|0\rangle$ to $|0\rangle|0\rangle$, $|j\rangle|0\rangle$ to a state proportional to $|j\rangle|\gamma^{j+}\rangle$ for $j \in \range{d}$, and $|j+d\rangle|0\rangle$ to a state proportional to $|j+d\rangle|\gamma^{j-}\rangle$ for $j \in \range{d}$. Suppose $\norm{|f\rangle},\norm{|\gamma^{j+}\rangle},\norm{|\gamma^{j-}\rangle}$ and $A_{j,j}$ for $j \in \range{d}$ are known.
% where the states $|f\rangle,|\gamma^{j+}\rangle,|\gamma^{j-}\rangle$ are defined as in \eq{f_inter} and \eq{g_inter}.
Define the parameter
\begin{equation}
  q: = \sqrt{\frac{\sum_{\|\bm{k}\|_{\infty}\le n} \sum_{j=1}^d [ \hat{f}^2_{\bm{k}}+(A_{j,j}\hat{\gamma}^{j+}_{\bm{k}})^2+(A_{j,j}\hat{\gamma}^{j-}_{\bm{k}})^2]}{\sum_{\|\bm{k}\|_{\infty}\le n} \sum_{j=1}^d |\hat{f}_{\bm{k}}+A_{j,j}\hat{\gamma}^{j+}_{\bm{k}}+A_{j,j}\hat{\gamma}^{j-}_{\bm{k}}|^2}}.
\end{equation}
Then the normalized quantum state
\begin{equation}
  |B\rangle \propto
\sum_{\|\bm{k}\|_{\infty}\le n} \sum_{j=1}^d (\hat{f}_{\bm{k}}+A_{j,j}\hat{\gamma}^{j+}_{\bm{k}}+A_{j,j}\hat{\gamma}^{j-}_{\bm{k}})|k_1\rangle\ldots|k_d\rangle,
\end{equation}
with coefficients defined as in \eq{f_coeff} and \eq{g_coeff},
can be prepared with gate and query complexity $O(qd^2\log n\log\log n)$.
\end{lemma}

\begin{proof}
Starting from the initial state $|0\rangle|0\rangle$, we first perform a unitary transformation $U$ satisfying
\begin{equation}
\begin{aligned}
  U|0\rangle=&\frac{\||f\rangle\|}{\sqrt{\||f\rangle\|^2
  +\sum_{j=1}^d\bigl(A_{j,j}^2\||\gamma^{j+}\rangle\|^2+A_{j,j}^2\||\gamma^{j-}\rangle\|^2\bigr)}}|0\rangle\\
  &+\sum_{j=1}^d\frac{A_{j,j}\||\gamma^{j+}\rangle\|}{\sqrt{\||f\rangle\|^2
  +\sum_{j=1}^d\bigl(A_{j,j}^2\||\gamma^{j+}\rangle\|^2+A_{j,j}^2\||\gamma^{j-}\rangle\|^2\bigr)}}|j\rangle\\
  &+\sum_{j=1}^d\frac{A_{j,j}\||\gamma^{j-}\rangle\|}{\sqrt{\||f\rangle\|^2
  +\sum_{j=1}^d\bigl(A_{j,j}^2\||\gamma^{j+}\rangle\|^2+A_{j,j}^2\||\gamma^{j-}\rangle\|^2\bigr)}}|j+d\rangle
\end{aligned}
\end{equation}
on the first register to obtain
\begin{equation}
  \frac{\||f\rangle\||0\rangle
  +A_{1,1}\||\gamma^{1+}\rangle\||1\rangle+\cdots+A_{d,d}\||\gamma^{d-}\rangle\| |2d\rangle}
  {\sqrt{\||f\rangle\|^2
  +\sum_{j=1}^d\bigl(A_{j,j}^2\||\gamma^{j+}\rangle\|^2+A_{j,j}^2\||\gamma^{j-}\rangle\|^2\bigr)}}|0\rangle.
\end{equation}
This can be done in time $O(2d+1)$ by standard techniques \cite{SBM06}. Then we apply $O_x$ and $O_f$ to obtain
\begin{equation}
  \begin{aligned}
  &|0\rangle|f\rangle+A_{1,1}|1\rangle|\gamma^{1+}\rangle+\cdots+A_{d,d}|2d\rangle|\gamma^{d-}\rangle, \\
&\quad\propto
\sum_{\|\bm{k}\|_{\infty},\|\bm{l}\|_{\infty}\le n} \phi_{\bm{k}}(\bm{\chi}_{\bm{l}})(\hat{f}_{\bm{k}}|0\rangle+A_{1,1}\hat{\gamma}^{1+}_{\bm{k}}|1\rangle+\cdots+A_{d,d}\hat{\gamma}^{d-}_{\bm{k}}|2d\rangle)|l_1\rangle\ldots|l_d\rangle,
\end{aligned}
\end{equation}
according to \eq{f_inter} and \eq{g_inter}. We then perform the $d$-dimensional inverse QSFT (for periodic boundary conditions) or inverse QCT (for non-periodic boundary conditions) on the last $d$ registers, obtaining
\begin{equation}
\sum_{\|\bm{k}\|_{\infty}\le n} (\hat{f}_{\bm{k}}|0\rangle+A_{1,1}\hat{\gamma}^{1+}_{\bm{k}}|1\rangle+\cdots+A_{d,d}\hat{\gamma}^{d-}_{\bm{k}}|2d\rangle)|k_1\rangle\ldots|k_d\rangle.
\end{equation}
Finally, observe that if we measure the first register in a basis containing the uniform superposition $|0\rangle+|1\rangle+\cdots+|2d\rangle$ (say, the Fourier basis) and obtain the outcome corresponding to the uniform superposition, we produce the state
\begin{equation}
\sum_{\|\bm{k}\|_{\infty}\le n} \sum_{j=1}^d (\hat{f}_{\bm{k}}+A_{j,j}\hat{\gamma}^{j+}_{\bm{k}}+A_{j,j}\hat{\gamma}^{j-}_{\bm{k}})|k_1\rangle\ldots|k_d\rangle.
\end{equation}
Since this outcome occurs with probability $1/q^2$, we can prepare this state with probability close to $1$ using $O(q)$ steps of amplitude amplification. According to \lem{qsft} and \lem{qct}, the $d$-dimensional (inverse) QSFT or QCT can be performed with gate complexity $O(d\log n\log\log n)$. Thus the total gate and query complexity is $O(qd^2\log n\log\log n)$.
\end{proof}

% Note that the parameter $q$, which depends on the inhomogeneity $f(\bm{x})$ and boundary conditions $\gamma(\bm{x})$, affects the success probability in the final measurement. Clearly $q$ is well defined since the system would have a trivial solution if the denominator of $q$ were $0$.
Alternatively, if it is possible to directly prepare the quantum state $|B\rangle$, then we may be able to avoid the factor of $q$ in the complexity of the overall algorithm.

%\amc{say more about the role of $q$? or maybe this is sufficient}
