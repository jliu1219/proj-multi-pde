% !TEX root = ../multi-pde-draft.tex

\section{Multi-dimensional spectral method}
\label{sec:spectral}

We now turn our attention to the spectral method for multi-dimensional PDEs.
Since interpolation facilitates constructing a straightforward linear system, we develop a quantum algorithm based on the \emph{pseudo-spectral method} \cite{Ghe07,STW11} with various boundary conditions.
After introducing the method, we discuss the quantum shifted Fourier transform and the quantum cosine transform (\sec{transform}), which are used as subroutines in our algorithm. Then we construct a linear system whose solution encodes the solution of the PDE (\sec{solver}), analyze its condition number (\sec{condition}), and consider the complexity of state preparation (\sec{state}). Finally, we present our main result in \sec{main}.

In the spectral approach, we approximate the exact solution $\hat u(\bm{x})$ by a linear combination of basis functions
\begin{equation}
u(\bm{x}) = \sum_{\|\bm{k}\|_{\infty}\le n}c_{\bm{k}}\phi_{\bm{k}}(\bm{x})
\label{eq:basis_expand}
\end{equation}
for some $n \in \Z^+$. Here $\bm{k}=(k_1,\ldots,k_d)$ with $k_j\in \rangez{n+1}:=\{0,1,\ldots,n\}$, $c_{\bm{k}}\in \C$, and
\begin{equation}
\phi_{\bm{k}}(\bm{x}) = \prod_{j=1}^d\phi_{k_j}(x_j), \quad j\in\range{d}.
\label{eq:basis_tensor}
\end{equation}

We choose different basis functions for the case of periodic boundary conditions and for the more general case of non-periodic boundary conditions. When the boundary conditions are periodic, the algorithm implementation is more straightforward, and in some cases (e.g., for the Poisson equation), can be faster. Specifically, for any $k_j \in \rangez{n+1}$ and $x_j \in [-1,1]$, we take
\begin{equation}
\phi_{k_j}(x_j) = \begin{cases}
e^{i(k_j-\left\lfloor n/2 \right\rfloor)\pi x_j}, & \text{periodic conditions},\\
T_{k_j}(x_j) := \cos(k_j\arccos x_j), & \text{non-periodic conditions}.
\end{cases}
\label{eq:basis_function}
\end{equation}
Here $T_k$ is the degree-$k$ Chebyshev polynomial of the first kind.

The coefficients $c_{\bm{k}}$ are determined by demanding that $u(\bm{x})$ satisfies the ODE and boundary conditions at a set of \emph{interpolation nodes} $\{\bm{\chi}_{\bm{l}}=(\chi_{l_1}, \ldots, \chi_{l_d})\}_{\|\bm{l}\|_{\infty\le n}}$ with $l_j\in \rangez{n+1}$, where
\begin{equation}
\chi_{l_j} = \begin{cases}
\frac{2 l_j}{n+1}-1, & \text{periodic conditions},\\
\cos\frac{\pi l_j}{n}, & \text{non-periodic conditions}.
\end{cases}
\label{eq:interpolation_nodes}
\end{equation}
Here $\{\frac{2 l}{n+1}-1: l \in \rangez{n+1}\}$ are called the \emph{uniform grid nodes}, and $\{\cos\frac{\pi l}{n} : l \in \rangez{n+1}\}$ are called the \emph{Chebyshev-Gauss-Lobatto quadrature nodes}.

We require the numerical solution $u(\bm{x})$ to satisfy
\begin{equation}
\L(u(\bm{\chi}_{\bm{l}}))=f(\bm{\chi}_{\bm{l}}), \quad \forall \, l_j\in\rangez{n+1},~ j\in\range{d}.
\label{eq:spectral_pde}
\end{equation}
We would like to be able to increase the accuracy of the approximation by increasing $n$, so that
\begin{equation}
\|\hat{u}(\bm{x})-u(\bm{x})\|\rightarrow0 \quad \text{as} \quad n\rightarrow\infty.
\end{equation}

The convergence behavior of the spectral method is related to the smoothness of the solution. For a solution in $C^{r+1}$, the spectral method approximates the solution with $n=\poly({1}/{\epsilon})$. Furthermore, if the solution is in $C^{\infty}$, the spectral method approximates the solution to within $\epsilon$ using only $n=\poly(\log(1/\epsilon))$ \cite{STW11}. Since we require $k_j\in\rangez{n+1}$ for all $j\in\range{d}$, we have $(n+1)^d$ terms in total. Consequently, a classical pseudo-spectral method solves multi-dimensional PDEs with complexity $\poly(\log^d(1/\epsilon))$. Such classical spectral methods rapidly become infeasible since the number of coefficients $(n+1)^d$ grows exponentially with $d$.

%\jpl{We might delete this paragraph.} The quantum spectral method \cite{CL19} provides a quantum algorithm for thisd problem with complexity $\poly((\log(1/\epsilon))^d)$, which is $\poly(\log(1/\epsilon))$ for first-order linear ODEs. \amc{doesn't \cite{CL19} \emph{only} treat ODEs?} The dense $(n+1)^d \times (n+1)^d$ linear system that appears in the spectral method gives the exponential dependence on $d$, even using the quantum linear system algorithm.

%Here we use the quantum spectral method \cite{CL19} as a subroutine in a high-precision algorithm, while we decompose the dense structure of the dense $(n+1)^d \times (n+1)^d$ linear system by a technique introduced in the next section.

Here we develop a quantum algorithm for multi-dimensional PDEs. The algorithm applies techniques from the quantum spectral method for ODEs \cite{CL19}. However, in the case of PDEs, the linear system to be solved is non-sparse. We address this difficulty using a quantum transform that restores sparsity.

