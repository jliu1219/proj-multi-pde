% !TEX root = ../multi-pde-draft.tex

\section{An example for solving Poisson's equation}
\label{app:Poisson}

In this appendix, we present an example of solving Poisson's equation in two dimensions using our algorithm. The Poisson equation is
\begin{align}
\Delta u(x_1,x_2)&=\left(\frac{\partial^2}{\partial x_1^2}+\frac{\partial^2}{\partial x_2^2}\right)u(x_1,x_2)=f(x_1,x_2), & (x_1,x_2) &\in \Omega=[-1, 1]^2.
\label{eq:Poisson_eq}
\end{align}

We consider two kinds of boundary value problems, as follows.
\begin{itemize}
  \item \emph{Periodic boundary conditions:}
  \begin{equation}
  \begin{aligned}
  u(x_1,x_2) &= u(x_1+2v,x_2) = u(x_1,x_2+2v) , & (x_1,x_2)& \in \Omega=[-1, 1]^2, v \in \Z\\
  u(0,0) &= \gamma.%\qquad (x_1,x_2) \in \partial\Omega^{-}_{j}:=\{\bm{x} \in \Omega | x_j=-1\} \quad \text{for each $j$}.
  \label{eq:period_cond}
  \end{aligned}
  \end{equation}

  \item \emph{Non-periodic boundary conditions:}
  \begin{equation}
  \begin{aligned}
  u(x_1,1) &= \gamma_N(x_1),  & u(1,x_2) &= \gamma_E(x_2),\\
  u(x_1,-1) &= \gamma_S(x_1), & u(-1,x_2) &= \gamma_W(x_2).
  \label{eq:non_period_cond}
  \end{aligned}
  \end{equation}
\end{itemize}

We first present the quantum Fourier spectral method to solve \eq{Poisson_eq} with the periodic conditions \eq{period_cond}. In particular, we choose $n=2$ in the specification of the linear system. The truncated Fourier series can be written as
\begin{equation}
u(x_1,x_2) = \sum_{k_1=0}^2\sum_{k_2=0}^2 c_{k_1,k_2}e^{i(k_1-1)\pi x_1}e^{i(k_2-1)\pi x_2} .
\label{eq:period_expand}
\end{equation}
We are given an oracle for preparing the state
\begin{equation}
\sum_{l_1=0}^2\sum_{l_2=0}^2f\left(\frac{2 l_1}{3}-1,\frac{2 l_2}{3}-1\right)|l_1\rangle|l_2\rangle
\label{eq:period_inter}
\end{equation}
that interpolates the uniform grid nodes \eq{interpolation_nodes}.
We first perform a multi-dimensional inverse QSFT on \eq{period_inter} to obtain
\begin{equation}
\sum_{k_1=0}^2\sum_{k_2=0}^2f_{k_1,k_2}|k_1\rangle|k_2\rangle,
\label{eq:Poisson_coeff}
\end{equation}
where $b_{k_1,k_2}$ are defined by \eq{f_coeff}. Then we apply the quantum linear system algorithm of \cite{CKS15} to solve the linear system
\begin{equation}
L_p|X\rangle=|B\rangle,
\label{eq:linear_system_Poisson}
\end{equation}
where the solution is
\begin{equation}
|X\rangle=  \sum_{k_1=0}^2\sum_{k_2=0}^2 c_{k_1,k_2}|k_1\rangle|k_2\rangle.
\label{eq:solution_Poisson}
\end{equation}
%\begin{equation}
%\sum_{\bm{j}\in\I} \frac{\partial^{\bm{j}}}{\partial \bm{x}^{\bm{j}}}u(\bm{x})=f(\bm{x}),\qquad \bm{x} \in \Omega=[-1, 1]^d
%\label{eq:Poisson_equiv}
%\end{equation}
%where
%\begin{equation}
%\bm{j} \in \I:=\{(2,0),(0,2)\},
%\end{equation}
%The matrix can be expressed as
%\begin{equation}
%L_p=\sum_{\bm{j}\in\I} \bm{D}^{\bm{j}}_{\bm{n}},
%\label{eq:matrix_sum_Poisson}
%\end{equation}
%where
%\begin{equation}
%\bm{D}^{\bm{j}}_{\bm{n}} \in \{D^{2}_n\otimes I^{\otimes d-1},I\otimes D^{2}_n\otimes I^{\otimes d-2},\ldots,I^{\otimes d-1}\otimes D^{2}_n\},
%\end{equation}
%and $D_n$ is defined by \eq{Fourier_Dn} or \eq{Chebyshev_Dn}. Here $d=2$, we directly have
According to \eq{coeff_linear_system},the discretized linear system from \eq{Poisson_eq} is
\begin{equation}
D^{2}_n\otimes I+I\otimes D^{2}_n,
\label{eq:Poisson_matrix}
\end{equation}
where the Fourier difference matrix $D_n$ is defined by \eq{Fourier_Dn} with $n=2$, namely
\begin{equation}
D_2=
  \begin{pmatrix}
    -i\pi & 0 & 0 \\
    0 & 0 & 0 \\
    0 & 0 & i\pi \\
  \end{pmatrix},
\end{equation}
so that
\begin{equation}
D_2^2=
  \begin{pmatrix}
    -\pi^2 & 0 & 0 \\
    0 & 0 & 0 \\
    0 & 0 & -\pi^2 \\
  \end{pmatrix}.
\end{equation}
Therefore, the matrix \eq{Poisson_matrix} is
\begin{equation}
D^{2}_2\otimes I+I\otimes D^{2}_2=
  \begin{pmatrix}
    -2\pi^2 &  &  &  &  &  &  &  &  \\
     & -\pi^2 &  &  &  &  &  &  &  \\
     &  & -2\pi^2 &  &  &  &  &  &  \\
     &  &  & -\pi^2 &  &  &  &  &  \\
     &  &  &  & 0 &  &  &  &  \\
     &  &  &  &  & -\pi^2 &  &  &  \\
     &  &  &  &  &  & -2\pi^2 &  &  \\
     &  &  &  &  &  &  & -\pi^2 &  \\
     &  &  &  &  &  &  &  & -2\pi^2 \\
  \end{pmatrix}.
\end{equation}
The rank of this matrix is $(n+1)^d-1$ with $d=2$, $n=2$. We use the boundary condition to complete the linear system:
\begin{equation}
  \begin{pmatrix}
    -2\pi^2 &  &  &  &  &  &  &  &  \\
     & -\pi^2 &  &  &  &  &  &  &  \\
     &  & -2\pi^2 &  &  &  &  &  &  \\
     &  &  & -\pi^2 &  &  &  &  &  \\
     &  &  & 1 & 1 & 1 &  &  &  \\
     &  &  &  &  & -\pi^2 &  &  &  \\
     &  &  &  &  &  & -2\pi^2 &  &  \\
     &  &  &  &  &  &  & -\pi^2 &  \\
     &  &  &  &  &  &  &  & -2\pi^2 \\
  \end{pmatrix}
  \begin{pmatrix}
    c_{0,0} \\
    c_{0,1} \\
    c_{0,2} \\
    c_{1,0} \\
    c_{1,1} \\
    c_{1,2} \\
    c_{2,0} \\
    c_{2,1} \\
    c_{2,2} \\
  \end{pmatrix}
=
  \begin{pmatrix}
    f_{0,0} \\
    f_{0,1} \\
    f_{0,2} \\
    f_{1,0} \\
    \gamma \\
    f_{1,2} \\
    f_{2,0} \\
    f_{2,1} \\
    f_{2,2} \\
  \end{pmatrix},
\end{equation}
where the additional linear equation comes from $u(0,0)=\sum_{k_1=0}^2\sum_{k_2=0}^2 c_{k_1,k_2}=\gamma$. In some problems, we might be directly given the value of $c_{1,1}$, say, $c_{1,1}=\gamma$. Then the linear system would be
\begin{equation}
  \begin{pmatrix}
    -2\pi^2 &  &  &  &  &  &  &  &  \\
     & -\pi^2 &  &  &  &  &  &  &  \\
     &  & -2\pi^2 &  &  &  &  &  &  \\
     &  &  & -\pi^2 &  &  &  &  &  \\
     &  &  &  & 1 &  &  &  &  \\
     &  &  &  &  & -\pi^2 &  &  &  \\
     &  &  &  &  &  & -2\pi^2 &  &  \\
     &  &  &  &  &  &  & -\pi^2 &  \\
     &  &  &  &  &  &  &  & -2\pi^2 \\
  \end{pmatrix}
  \begin{pmatrix}
    c_{0,0} \\
    c_{0,1} \\
    c_{0,2} \\
    c_{1,0} \\
    c_{1,1} \\
    c_{1,2} \\
    c_{2,0} \\
    c_{2,1} \\
    c_{2,2} \\
  \end{pmatrix}
=
  \begin{pmatrix}
    f_{0,0} \\
    f_{0,1} \\
    f_{0,2} \\
    f_{1,0} \\
    \gamma \\
    f_{1,2} \\
    f_{2,0} \\
    f_{2,1} \\
    f_{2,2} \\
  \end{pmatrix}.
\end{equation}

We now present the quantum Chebyshev spectral method to solve \eq{Poisson_eq} with non-periodic conditions \eq{non_period_cond}. Similarly, we choose $n=3$ in the specification of the linear system. The truncated Chebyshev series of the solution can be written as
\begin{equation}
u(x_1,x_2) = \sum_{k_1=0}^3\sum_{k_2=0}^3 c_{k_1,k_2}T_{k_1}(x_1)T_{k_2}(x_2).
\label{eq:non_period_expand}
\end{equation}
We are given an oracle for preparing the state
\begin{equation}
\sum_{l_1=0}^3\sum_{l_2=0}^3f\left(\cos\frac{\pi l_1}{3},\cos\frac{\pi l_2}{3}\right)|l_1\rangle|l_2\rangle
\label{eq:non_period_inter}
\end{equation}
that interpolates the Chebyshev-Gauss-Lobatto quadrature nodes specified in \eq{interpolation_nodes}.
We first perform the multi-dimensional inverse QCT on \eq{period_inter} to obtain \eq{Poisson_coeff}, where $f_{k_1,k_2}$ are defined by \eq{f_coeff}. Then we apply the quantum linear system algorithm of \cite{CKS15} to solve the linear system \eq{linear_system_Poisson} with the solution \eq{solution_Poisson}. The discretized linear system from \eq{Poisson_eq} is \eq{Poisson_matrix}, where the Chebyshev difference matrix $D_n$ is defined by \eq{Chebyshev_Dn} with $n=3$, namely
\begin{equation}
D_3=
  \begin{pmatrix}
    0 & 1 & 0 & 3 \\
    0 & 0 & 4 & 0 \\
    0 & 0 & 0 & 6 \\
    0 & 0 & 0 & 0 \\
  \end{pmatrix},
\end{equation}
and
\begin{equation}
D_3^2=
  \begin{pmatrix}
    0 & 0 & 4 & 0 \\
    0 & 0 & 0 & 24 \\
    0 & 0 & 0 & 0 \\
    0 & 0 & 0 & 0 \\
  \end{pmatrix}.
\end{equation}
Notice that the rank of $D_n^2$ is $n-1$, which implies the second derivative for $d=1$ can be represented as
\begin{equation}
u''(x) = c''_0T_0(x)+c''_1T_1(x) = 4c_2T_0(x)+24c_3T_1(x),
\end{equation}
where the truncation order of $u''(x)$ is $n-2$, and the coefficients $c''_0,\ldots,c''_{n-2}$ are determined by $c_2,\ldots,c_n$. Similarly for the case $d=2$, the coefficients of $\Delta u(x)$ are determined by
\begin{equation}
\begin{aligned}
c''_{00} &= 4c_{02}+4c_{20}, &
c''_{01} &= 24c_{03}+4c_{21}, &
c''_{02} &= 4c_{22}, &
c''_{03} &= 4c_{23}, \\
c''_{10} &= 4c_{12}+24c_{30}, &
c''_{11} &= 24c_{13}+24c_{31}, &
c''_{12} &= 24c_{32}, &
c''_{13} &= 24c_{33}, \\
c''_{20} &= 4c_{22}, &
c''_{21} &= 24c_{23},\\
c''_{30} &= 4c_{32}, &
c''_{33} &= 24c_{33},
\end{aligned}
\end{equation}
so the matrix $D^{2}_3\otimes I+I\otimes D^{2}_3$ gives the linear system
\begin{equation}
  \begin{pmatrix}
     &  & 4 &  &  &  &  &  & 4 &  &  &  &  &  &  &  \\
     &  &  & 24 &  &  &  &  &  & 4 &  &  &  &  &  &  \\
     &  &  &  &  &  &  &  &  &  & 4 &  &  &  &  &  \\
     &  &  &  &  &  &  &  &  &  &  & 4 &  &  &  &  \\
     &  &  &  &  &  & 4 &  &  &  &  &  & 24 &  &  &  \\
     &  &  &  &  &  &  & 24 &  &  &  &  &  & 24 &  &  \\
     &  &  &  &  &  &  &  &  &  &  &  &  &  & 24 &  \\
     &  &  &  &  &  &  &  &  &  &  &  &  &  &  & 24 \\
     &  &  &  &  &  &  &  &  &  & 4 &  &  &  &  &  \\
     &  &  &  &  &  &  &  &  &  &  & 24 &  &  &  &  \\
     &  &  &  &  &  &  &  &  &  &  &  &  &  & 4 &  \\
     &  &  &  &  &  &  &  &  &  &  &  &  &  &  & 24 \\
  \end{pmatrix}
  \begin{pmatrix}
    c_{0,0} \\
    c_{0,1} \\
    c_{0,2} \\
    c_{0,3} \\
    c_{1,0} \\
    c_{1,1} \\
    c_{1,2} \\
    c_{1,3} \\
    c_{2,0} \\
    c_{2,1} \\
    c_{2,2} \\
    c_{2,3} \\
    c_{3,0} \\
    c_{3,1} \\
    c_{3,2} \\
    c_{3,3} \\
  \end{pmatrix}
=
  \begin{pmatrix}
    f_{0,0} \\
    f_{0,1} \\
    f_{0,2} \\
    f_{0,3} \\
    f_{1,0} \\
    f_{1,1} \\
    f_{1,2} \\
    f_{1,3} \\
    f_{2,0} \\
    f_{2,1} \\
    f_{3,0} \\
    f_{3,1} \\
  \end{pmatrix}.
\label{eq:diff_system}
\end{equation}
We now use the boundary conditions to complete the linear system. The truncated Chebyshev series of the solution can be written as
\begin{equation}
\begin{aligned}
\gamma_N(x_1) &= \sum_{k_1=0}^2 {g_N}_{k_1}T_{k_1}(x_1),\\
\gamma_S(x_1) &= \sum_{k_1=0}^2 {g_S}_{k_1}T_{k_1}(x_1),\\
\gamma_E(x_2) &= \sum_{k_2=0}^2 {g_E}_{k_1}T_{k_1}(x_2),\\
\gamma_W(x_2) &= \sum_{k_2=0}^2 {g_W}_{k_1}T_{k_1}(x_2).
\label{eq:boundary_expand}
\end{aligned}
\end{equation}
We are given an oracle for preparing the state by interpolating the Chebyshev-Gauss-Lobatto quadrature nodes specified in \eq{interpolation_nodes}
\begin{equation}
\begin{aligned}
&\sum_{l_1=0}^3\gamma_N\left(\cos\frac{\pi l_1}{3}\right)|l_1\rangle, &
&\sum_{l_2=0}^3\gamma_E\left(\cos\frac{\pi l_2}{3}\right)|l_2\rangle,\\
&\sum_{l_1=0}^3\gamma_S\left(\cos\frac{\pi l_1}{3}\right)|l_1\rangle, &
&\sum_{l_2=0}^3\gamma_W\left(\cos\frac{\pi l_2}{3}\right)|l_2\rangle.
\label{eq:boundary_inter}
\end{aligned}
\end{equation}
We perform the multi-dimensional inverse QCT on \eq{boundary_inter} to obtain
\begin{equation}
\begin{aligned}
&\sum_{k_1=0}^3{g_N}_{k_1}|k_1\rangle, &
&\sum_{k_2=0}^3{g_E}_{k_2}|k_2\rangle, \\
&\sum_{k_1=0}^3{g_S}_{k_1}|k_1\rangle, &
&\sum_{k_2=0}^3{g_W}_{k_2}|k_2\rangle,\\
\label{eq:boundary_coeff}
\end{aligned}
\end{equation}
where $a_{k_1,k_2}$ are defined by \eq{g_coeff}. The linear system from the boundary conditions is
\begin{equation}
  \begin{pmatrix}
    1 & 1 & 1 & 1 &  &  &  &  &  &  &  &  &  &  &  &  \\
     &  &  &  & 1 & 1 & 1 & 1 &  &  &  &  &  &  &  &  \\
     &  &  &  &  &  &  &  & 1 & 1 & 1 & 1 &  &  &  &  \\
     &  &  &  &  &  &  &  &  &  &  &  & 1 & 1 & 1 & 1 \\
    1 & -1 & 1 & -1 &  &  &  &  &  &  &  &  &  &  &  &  \\
     &  &  &  & 1 & -1 & 1 & -1 &  &  &  &  &  &  &  &  \\
     &  &  &  &  &  &  &  & 1 & -1 & 1 & -1 &  &  &  &  \\
     &  &  &  &  &  &  &  &  &  &  &  & 1 & -1 & 1 & -1 \\
    1 &  &  &  & 1 &  &  &  & 1 &  &  &  & 1 &  &  &  \\
     & 1 &  &  &  & 1 &  &  &  & 1 &  &  &  & 1 &  &  \\
     &  & 1 &  &  &  & 1 &  &  &  & 1 &  &  &  & 1 &  \\
     &  &  & 1 &  &  &  & 1 &  &  &  & 1 &  &  &  & 1 \\
    1 &  &  &  & -1 &  &  &  & 1 &  &  &  & -1 &  &  &  \\
     & 1 &  &  &  & -1 &  &  &  & 1 &  &  &  & -1 &  &  \\
     &  & 1 &  &  &  & -1 &  &  &  & 1 &  &  &  & -1 &  \\
     &  &  & 1 &  &  &  & -1 &  &  &  & 1 &  &  &  & -1 \\
  \end{pmatrix}
  \begin{pmatrix}
    c_{0,0} \\
    c_{0,1} \\
    c_{0,2} \\
    c_{0,3} \\
    c_{1,0} \\
    c_{1,1} \\
    c_{1,2} \\
    c_{1,3} \\
    c_{2,0} \\
    c_{2,1} \\
    c_{2,2} \\
    c_{2,3} \\
    c_{3,0} \\
    c_{3,1} \\
    c_{3,2} \\
    c_{3,3} \\
  \end{pmatrix}
=
  \begin{pmatrix}
    {g_N}_{0} \\
    {g_N}_{1} \\
    {g_N}_{2} \\
    {g_N}_{3} \\
    {g_S}_{0} \\
    {g_S}_{1} \\
    {g_S}_{2} \\
    {g_S}_{3} \\
    {g_E}_{0} \\
    {g_E}_{1} \\
    {g_E}_{2} \\
    {g_E}_{3} \\
    {g_W}_{0} \\
    {g_W}_{1} \\
    {g_W}_{2} \\
    {g_W}_{3} \\
  \end{pmatrix}.
\label{eq:boundary_system}
\end{equation}
Adding the two linear systems \eq{diff_system} and \eq{boundary_system} together, we obtain a full-rank linear system
\begin{equation}
\overline{D}^{(2)}_3\otimes I+I\otimes \overline{D}^{(2)}_3,
\label{eq:complete_matrix}
\end{equation}
where
\begin{equation}
\overline{D}_3^{(2)}=
  \begin{pmatrix}
    0 & 0 & 4 & 0 \\
    0 & 0 & 0 & 24 \\
    1 & -1 & 1 & -1 \\
    1 & 1 & 1 & 1 \\
  \end{pmatrix}.
\end{equation}
In summary, the linear system including the differential equations and the boundary conditions is
\begin{equation}
  \small
  \begin{pmatrix}
     &  & 4 &  &  &  &  &  & 4 &  &  &  &  &  &  &  \\
     &  &  & 24 &  &  &  &  &  & 4 &  &  &  &  &  &  \\
    1 & -1 & 1 & -1 &  &  &  &  &  &  & 4 &  &  &  &  &  \\
    1 & 1 & 1 & 1 &  &  &  &  &  &  &  & 4 &  &  &  &  \\
     &  &  &  &  &  & 4 &  &  &  &  &  & 24 &  &  &  \\
     &  &  &  &  &  &  & 24 &  &  &  &  &  & 24 &  &  \\
     &  &  &  & 1 & -1 & 1 & -1 &  &  &  &  &  &  & 24 &  \\
     &  &  &  & 1 & 1 & 1 & 1 &  &  &  &  &  &  &  & 24 \\
    1 &  &  &  & -1 &  &  &  & 1 &  & 4 &  & -1 &  &  &  \\
     & 1 &  &  &  & -1 &  &  &  & 1 &  & 24 &  & -1 &  &  \\
     &  & 1 &  &  &  & -1 &  & 1 & -1 & 2 & -1 &  &  & -1 &  \\
     &  &  & 1 &  &  &  & -1 & 1 & 1 & 1 & 2 &  &  &  & -1 \\
    1 &  &  &  & 1 &  &  &  & 1 &  &  &  & 1 &  & 4 &  \\
     & 1 &  &  &  & 1 &  &  &  & 1 &  &  &  & 1 &  & 24 \\
     &  & 1 &  &  &  & 1 &  &  &  & 1 &  & 1 & -1 & 2 & -1 \\
     &  &  & 1 &  &  &  & 1 &  &  &  & 1 & 1 & 1 & 1 & 2 \\
  \end{pmatrix}
    \begin{pmatrix}
    c_{0,0} \\
    c_{0,1} \\
    c_{0,2} \\
    c_{0,3} \\
    c_{1,0} \\
    c_{1,1} \\
    c_{1,2} \\
    c_{1,3} \\
    c_{2,0} \\
    c_{2,1} \\
    c_{2,2} \\
    c_{2,3} \\
    c_{3,0} \\
    c_{3,1} \\
    c_{3,2} \\
    c_{3,3} \\
  \end{pmatrix}
=
  \begin{pmatrix}
    f_{0,0} \\
    f_{0,1} \\
    f_{0,2}+{g_S}_{0} \\
    f_{0,3}+{g_N}_{0} \\
    f_{1,0} \\
    f_{1,1} \\
    f_{1,2}+{g_S}_{1} \\
    f_{1,3}+{g_N}_{1} \\
    f_{2,0}+{g_W}_{0} \\
    f_{2,1}+{g_W}_{1} \\
    {g_W}_{2}+{g_S}_{2} \\
    {g_W}_{3}+{g_N}_{2} \\
    f_{3,0}+{g_E}_{0} \\
    f_{3,1}+{g_E}_{1} \\
    {g_E}_{2}+{g_S}_{3} \\
    {g_E}_{3}+{g_N}_{3} \\
  \end{pmatrix}.
\label{eq:complete_system}
\end{equation}
