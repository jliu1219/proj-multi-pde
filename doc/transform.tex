% !TEX root = ../multi-pde-draft.tex

\subsection{Quantum shifted Fourier transform and quantum cosine transform}
\label{sec:transform}

% Quantum computers are able to efficiently achieve linear transformations exponentially faster than classical computers for some special structures (even possible for non-sparse transformation).
The well-known \emph{quantum Fourier transform} (QFT) can be regarded as an analogue of the discrete Fourier transform (DFT) acting on the amplitudes of a quantum state. The QFT maps the $(n+1)$-dimensional quantum state $v=(v_0,v_1,\ldots,v_n)\in\C^{n+1}$ to the state $\hat{v}=(\hat{v}_0,\hat{v}_1,\ldots,\hat{v}_n)\in\C^{n+1}$ with
\begin{equation}
\hat{v}_l = \frac{1}{\sqrt{n+1}}\sum_{k=0}^{n}\exp\Bigl(\frac{2\pi ikl}{n+1}\Bigr)v_k,\quad l\in\rangez{n+1}.
\label{eq:qft_rule}
\end{equation}
In other words, the QFT is the unitary transform
\begin{equation}
F_n:=\frac{1}{\sqrt{n+1}}\sum_{k,l=0}^n \exp\Bigl(\frac{2\pi ikl}{n+1}\Bigr)|l\rangle\langle k|.
\label{eq:qft_matrix}
\end{equation}
%The classical DFT on $(n+1)$-dimensional vectors takes $\Theta(n\log n)$ gates, while the QFT on $(n+1)$-dimensional quantum states can be implemented with only $O(\log n\log\log n)$ gates \cite{HH00}. \amc{not sure if this is a useful comparison}\jpl{I just imply the exponential speed-up for dense linear system comes from QFT and QCT, but this paragraph is not necessary.}

Here we also consider the \emph{quantum shifted Fourier transform} (QSFT), an analogue of the classical shifted discrete Fourier transform, which maps $v \in \C^{n+1}$ to $\hat v \in \C^{n+1}$ with
\begin{equation}
\hat{v}_l = \frac{1}{\sqrt{n+1}}\sum_{k=0}^{n}\exp\Bigl(\frac{2\pi i(k-\left\lfloor n/2 \right\rfloor)(l-(n+1)/2)}{n+1}\Bigr)v_k,\quad l\in\rangez{n+1}.
\label{eq:qsft_rule}
\end{equation}
In other words, the QSFT is the unitary transform
\begin{equation}
F^s_n:=\frac{1}{\sqrt{n+1}}\sum_{k,l=0}^n \exp\Bigl(\frac{2\pi i(k-\left\lfloor n/2 \right\rfloor)(l-(n+1)/2)}{n+1}\Bigr)|l\rangle\langle k|.
\label{eq:qsft_matrix}
\end{equation}

We define the multi-dimensional QSFT by the tensor product, namely
\begin{equation}
{\bm{F}}^s_n:=\frac{1}{\sqrt{(n+1)^d}}\sum_{\|\bm{k}\|_{\infty},\|\bm{l}\|_{\infty}\le n} \prod_{j=1}^d\exp\Bigl(\frac{2\pi i(k_j-\left\lfloor n/2 \right\rfloor)(l_j-(n+1)/2)}{n+1}\Bigr)|l_1\rangle\ldots|l_d\rangle\langle k_1|\ldots\langle k_d|,
\label{eq:mqsft_matrix}
\end{equation}
where $\bm{k}=(k_1,\ldots,k_d)$ and $\bm{l}=(l_1,\ldots,l_d)$ are $d$-dimensional vectors with $k_j, l_j\in \rangez{n}$.

The QSFT can be efficiently implemented as follows:

\begin{lemma}\label{lem:qsft}
The QSFT $F_n^s$ defined by \eq{qsft_matrix} can be performed with gate complexity $O(\log n\log\log n)$. More generally, the $d$-dimensional QSFT ${\bm F}_n^s$ defined by \eq{mqsft_matrix} can be performed with gate complexity $O(d \log n\log\log n)$.
\end{lemma}

\begin{proof}
The unitary matrix $F^s_n$ can be written as the product of three unitary matrices
\begin{equation}
F^s_n=S_nF_nR_n,
\end{equation}
where
\begin{equation}
R_n=\sum_{k=0}^n \exp\Bigl(-\frac{2\pi i k(n+1)/2}{n+1}\Bigr)|k\rangle\langle k|
\end{equation}
and
\begin{equation}
S_n=\sum_{l=0}^n \exp\Bigl(-\frac{2\pi i \left\lfloor n/2 \right\rfloor (l-(n+1)/2)}{n+1}\Bigr)|l\rangle\langle l|.
\end{equation}
It is well known that $F_n$ can be implemented with gate complexity $O(\log n\log\log n)$, and it is straightforward to implement $R_n$ and $S_n$ with gate complexity $O(\log n)$. Thus the total complexity is $O(\log n\log\log n)$.

We rewrite $\bm{v}$ in the form
\begin{equation}
\bm{v}=\sum_{\|\bm{k}\|_{\infty}\le n}\bm{v}_{\bm{k}}|k_1\rangle\ldots|k_d\rangle,
\end{equation}
where $\bm{v}_{\bm{k}}\in\C$ with $\bm{k}=(k_1,\ldots,k_d)$, and each $k_j\in \rangez{n}$ for $j\in \range{d}$.
The unitary matrix ${\bm{F}}^s_n$ can be written as the tensor product
\begin{equation}
{\bm{F}}^s_n=\bigotimes_{j=1}^d F^s_n.
\end{equation}
Performing the multi-dimensional QSFT is equivalent to performing the one-dimensional QSFT on each register. Thus, the gate complexity of performing ${\bm{F}}^s_n$ is $O(d\log n\log\log n)$.
\end{proof}

Another efficient quantum transformation is the \emph{quantum cosine transform} (QCT) \cite{KR01,RPB99}. The QCT can be regarded as an analogue of the discrete cosine transform (DCT). The QCT maps $v\in\C^{n+1}$ to $\hat{v}\in\C^{n+1}$  with
\begin{equation}
\hat{v}_l = \sqrt{\frac{2}{n}}\sum_{k=0}^{n} \delta_k\delta_l\cos\frac{kl\pi}{n}v_k,\quad l\in\rangez{n+1},
\label{eq:qct_rule}
\end{equation}
where
\begin{equation}
\delta_l := \begin{cases}
\frac{1}{\sqrt{2}} & l=0,n\\
1 & l\in\range{n-1}.
\end{cases}
\end{equation}
In other words, the QCT is the orthogonal transform
\begin{equation}
C_n:=\sqrt{\frac{2}{n}}\sum_{k,l=0}^{n} \delta_l\delta_k\cos\frac{kl\pi}{n}|l\rangle\langle k|.
\label{eq:qct_matrix}
\end{equation}

%Here we introduce \emph{quantum modified Fourier transform} (QMCT), which maps $v \in \C^{n+1}$ to $\hat v \in \C^{n+1}$ with
%\begin{equation}
%\hat{v}_l = \sqrt{\frac{2}{n}}\sum_{k=0}^{n} \cos\frac{kl\pi}{n}v_k,\quad l\in\rangez{n+1},
%\label{eq:qmct_rule}
%\end{equation}
%In other words, the QMCT is a non-unitary transform
%\begin{equation}
%C^m_n:=\sqrt{\frac{2}{n}}\sum_{k,l=0}^{n} \cos\frac{kl\pi}{n}|l\rangle\langle k|.
%\label{eq:qmct_matrix}
%\end{equation}
%
Again we define the multi-dimensional QCT by the tensor product, namely
\begin{equation}
{\bm{C}}_n:=\sqrt{\Bigl(\frac{2}{n}\Bigr)^d}\sum_{\|\bm{k}\|_{\infty},\|\bm{l}\|_{\infty}\le n} \prod_{j=1}^d\delta_{k_j}\delta_{l_j}\cos\frac{k_jl_j\pi}{n}|l_1\rangle\ldots|l_d\rangle\langle k_1|\ldots\langle k_d|,
\label{eq:mqct_matrix}
\end{equation}
where $\bm{k}=(k_1,\ldots,k_d)$ and $\bm{l}=(l_1,\ldots,l_d)$ are $d$-dimensional vectors with $k_j, l_j\in \rangez{n+1}$.

The classical DCT on $(n+1)$-dimensional vectors takes $\Theta(n\log n)$ gates, while the QCT on $(n+1)$-dimensional quantum states can be implemented with complexity $\poly(\log n)$. According to \cite[Theorem 1]{KR01}, the gate complexity of performing $C_n$ is $O(\log^2 n)$. We observe that this can be improved as follows.

\begin{lemma}\label{lem:qct}
The quantum cosine transform $C_n$ defined by \eq{qct_matrix} can be performed with gate complexity $O(\log n\log\log n)$. More generally, the multi-dimensional QCT $\bm{C}_n$ defined by \eq{mqct_matrix} can be performed with gate complexity $O(d\log n\log\log n)$.
\end{lemma}

\begin{proof}
%The matrix $C^m_n$ can be written as the product of two unitary matrices
%\begin{equation}
%C^m_n=M_nC_nM_n,
%\end{equation}
%where
%\begin{equation}
%M_n=\sum_{k=0}^n \frac{1}{\delta_k}|k\rangle\langle k|,
%\end{equation}
%The gate complexity of performing $M_n$ is $O(\log n)$.
According to the quantum circuit in Figure 2 of \cite{KR01}, $C_n$ can be decomposed into a QFT $F_{n+1}$, a permutation
\begin{equation}
P_n=
  \begin{pmatrix}
     &  &  &  & 1 \\
    1 &  &  &  & \\
     & 1 &  &  & \\
     &  & \ddots  & & \\
     &  &  &  1 & \\
  \end{pmatrix},
\end{equation}
and additional operations with $O(1)$ cost. The QFT $F_{n+1}$ has gate complexity $O(\log n\log\log n)$. We then consider an alternative way to implement $P_n$ that improves over the approach in \cite{PRB99}.

The permutation $P_n$ can be decomposed as
\begin{equation}
P_n=F_nT_nF_n^{-1},
\end{equation}
where $F_n$ is the Fourier transform \eq{qft_matrix} and $T_n = \sum_{k=0}^n e^{-\frac{2\pi i k}{n+1}} |k\rangle\langle k|$ is diagonal. The gate complexities of performing $F_n$ and $T_n$ are $O(\log n\log\log n)$ and $O(\log n)$, respectively. It follows that $C_n$ can be implemented with circuit complexity $O(\log n\log\log n)$.

The matrix ${\bm{C}}_n$ can be written as the tensor product
\begin{equation}
{\bm{C}}_n=\bigotimes_{j=1}^d C_n.
\end{equation}
As in \lem{qsft}, performing the multi-dimensional QCT is equivalent to performing a QCT on each register. Thus, the gate complexity of performing ${\bm{C}}_n$ is $O(d\log n\log\log n)$.
\end{proof}
