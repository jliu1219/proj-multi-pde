\documentclass[11pt]{article}

\usepackage[margin=1in]{geometry}
\DeclareMathAlphabet{\mathbbold}{U}{bbold}{m}{n}

\usepackage{colonequals}
%\usepackage{thm-restate}
\usepackage{braket} %Provides \Bra{}, \Ket{}, etc.

% \usepackage[pagebackref]{hyperref}
\usepackage{hyperref}
\hypersetup{
    bookmarksnumbered=true, % If Acrobat bookmarks are requested, include section numbers
    unicode=false, % non-Latin characters in Acrobat�s bookmarks
    pdfstartview={FitH}, % fits the width of the page to the window
    pdftitle={Quantum algorithms and lower bounds for convex optimization}, % title
    pdfauthor={Andrew M. Childs, Aaron Ostrander, Jin-Peng Liu}, % author
    pdfsubject={}, % subject of the document
    pdfcreator={}, % creator of the document
    pdfproducer={}, % producer of the document
    pdfkeywords={}, % list of keywords
    pdfnewwindow=true, % links in new window
    colorlinks=true, % false: boxed links; true: colored links
    linkcolor=blue, % color of internal links
    citecolor=blue, % color of links to bibliography
    filecolor=blue, % color of file links
    urlcolor=blue % color of external links
}

\usepackage{times}
\usepackage[scaled=.90]{helvet}

\usepackage{titling}
\usepackage[compact]{titlesec}

\usepackage{amssymb,amsmath,amsthm,amsfonts}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage[numbers,comma,sort&compress]{natbib}
\usepackage{authblk}
\usepackage{graphicx}
\usepackage[font=small]{caption}
\usepackage[labelformat=simple]{subcaption}
\usepackage{float}
\usepackage[ruled,vlined,linesnumbered]{algorithm2e}
\usepackage{physics}
\usepackage{footnote}
\usepackage{xcolor}

\usepackage{float,multirow}

\makeatletter
\renewcommand{\paragraph}{%
  \@startsection{paragraph}{4}%
  {\z@}{2.25ex \@plus .5ex \@minus .3ex}{-1em}%
  {\normalfont\normalsize\bfseries}%
}
\makeatother

%-----------------------------------------------------------------------------%
% Theorem-like environments and related macros:
%-----------------------------------------------------------------------------%

\newtheoremstyle{newstyle}
{0.5ex} %Aboveskip
{0.4ex} %Below skip
{\itshape} %Body font e.g.\mdseries,\bfseries,\scshape,\itshape
{} %Indent
{\bfseries} %Head font e.g.\bfseries,\scshape,\itshape
{.} %Punctuation afer theorem header
{ } %Space after theorem header
{} %Heading

\theoremstyle{newstyle}

\newtheorem{theorem}{Theorem}%[section]
\newtheorem{lemma}[theorem]{Lemma}%[section]
\newtheorem{proposition}[theorem]{Proposition}%[section]
\newtheorem{corollary}[theorem]{Corollary}%[section]

\theoremstyle{definition}
\newtheorem{problem}[theorem]{Problem}%[section]

\newcommand{\eqn}[1]{(\ref{eqn:#1})}
\newcommand{\prb}[1]{(\ref{prb:#1})}
\newcommand{\eq}[1]{\hyperref[eq:#1]{(\ref*{eq:#1})}}
\renewcommand{\sec}[1]{\hyperref[sec:#1]{Section~\ref*{sec:#1}}}
\newcommand{\thm}[1]{\hyperref[thm:#1]{Theorem~\ref*{thm:#1}}}
\newcommand{\lem}[1]{\hyperref[lem:#1]{Lemma~\ref*{lem:#1}}}
\newcommand{\prop}[1]{\hyperref[prop:#1]{Proposition~\ref*{prop:#1}}}
\newcommand{\cor}[1]{\hyperref[cor:#1]{Corollary~\ref*{cor:#1}}}
\newcommand{\fig}[1]{\hyperref[fig:#1]{Figure~\ref*{fig:#1}}}
\newcommand{\tab}[1]{\hyperref[tab:#1]{Table~\ref*{tab:#1}}}
\newcommand{\alg}[1]{\hyperref[alg:#1]{Algorithm~\ref*{alg:#1}}}
\newcommand{\app}[1]{\hyperref[app:#1]{Appendix~\ref*{app:#1}}}

%-----------------------------------------------------------------------------%
% Macros:
%-----------------------------------------------------------------------------%

\newcommand{\C}{{\mathbb{C}}}
\newcommand{\R}{{\mathbb{R}}}

\DeclareMathOperator{\poly}{poly}

\renewcommand{\>}{\rangle}
\newcommand{\<}{\langle}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setlength{\droptitle}{-21mm}

\title{High-precision quantum algorithms for partial differential equations}

\usepackage{authblk}
\author[1]{Andrew M. Childs}
\author[1]{Jin-Peng Liu}
\author[1]{Aaron Ostrander}
\affil[1]{QuICS, University of Maryland}

\begin{document}

\date{}
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\renewcommand{\abstractname}{\vspace*{-20mm}}

\begin{abstract}
Quantum computers can produce a quantum encoding of the solution of a system of differential equations exponentially faster than a classical algorithm can produce an explicit description. However, while high-precision quantum algorithms for linear ordinary differential equations are well established, the best previous quantum algorithms for linear partial differential equations (PDEs) have complexity $\poly(1/\epsilon)$ where $\epsilon$ is the error tolerance. By developing quantum algorithms based on adaptive-order finite difference methods and spectral methods, we improve the complexity of quantum algorithms for linear PDEs to be $\poly(d, \log(1/\epsilon))$, where $d$ is the spatial dimension. Our algorithms apply high-precision quantum linear system algorithms to systems whose condition numbers and approximation errors we bound. We develop a finite difference algorithm for the Poisson equation and a spectral algorithm for more general second-order elliptic equations.
\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Many scientific problems involve partial differential equations (PDEs). Prominent examples include Maxwell's equations for electromagnetism, Boltzmann's equation and the Fokker-Planck equation in thermodynamics, and Schr{\"o}dinger's equation in continuum quantum mechanics. While models of physics are typically studied in a constant number of spatial dimensions, it is also natural to study high-dimensional PDEs, such as to model systems with many interacting particles. Classical numerical methods have complexity that grows exponentially in the dimension, a phenomenon sometimes called the curse of dimensionality. This is a major challenge for attempts to solve PDEs on classical computers.

A common approach to solving PDEs on a digital computer is the finite difference method (FDM). In this approach, we discretize space into a rectangular lattice, solve a system of linear equations that approximates the PDE on the lattice, and output the solution on those grid points. If each spatial coordinate has $n$ discrete values, then $n^d$ points are needed to discretize a $d$-dimensional problem. Simply outputting the solution on these grid points takes time $\Omega(n^d)$ classically.

Beyond uniform grids, the sparse grid technique \cite{Smo63} has been applied to reduce the time and space complexity of outputting the solution to $O(n\log^d n)$ \cite{BG04,Zen91}. While this is a significant improvement, it still scales exponentially in $d$. It can be shown that for a grid-based approach this complexity is optimal with respect to certain norms \cite{BG04}. Reference~\cite{BG04} proposes alternative sparse grid algorithms whose complexities scale linearly with $n$ but exponentially with $d$. Another grid-based method is the finite element method (FEM), where the differential equation is multiplied by functions with local support (restricted by the grid) and then integrated. This produces a set of equation which the solution must satisfy, which are then used to approximate the solution. The finite volume method (FVM) considers a grid dividing space into volumes/cells. The field is integrated over these volumes to create auxiliary variables, and relations between these variables are derived from the differential equation.

An alternative to grid methods is the concept of \emph{spectral methods} \cite{Ghe07,STW11}. Spectral methods use linear combinations of basis functions (such as Fourier basis states or Chebyshev polynomials) to globally approximate the solution. These basis functions allow the construction of a linear system whose solution approximates the solution of the PDE.

These classical algorithms often consider the problem of outputting the solution at $N$ points in space, which clearly requires $\Omega(N)$ space and time. Quantum algorithms often (though not always) consider the alternative problem of outputting a quantum state proportional to such a vector, which requires $\Omega (\log N)$ space---and provides more limited access to the solution---but can potentially be done in only $\poly(\log N)$ time.

The fact that quantum states can efficiently encode exponentially long vectors has also been leveraged for the development of quantum linear system algorithms (QLSAs) \cite{HHL08,Amb12,CKS15}. For a linear system $A \vec x = \vec b$, a QLSA outputs a quantum state proportional to the solution $\vec x$. To learn information about the solution $\vec x$, the output of the QLSA must be post-processed. For example, to output all the entries of an $N$-dimensional vector $\vec x$ given a quantum state $\ket{x}$ proportional to it, even a quantum computer needs time and space $\Omega(N)$.

Because linear systems are often used in classical algorithms for PDEs such as those described above, it is natural to consider their quantum counterparts. Clader, Jacobs, and Sprouse~\cite{CJS13} give a heuristic algorithm for using sparse preconditioners and QLSAs to solve a linear system constructed using the FEM for Maxwell's equations. The state output by the QLSA is then post-processed to compute electromagnetic scattering cross-sections.

In subsequent work, Montanaro and Pallister~\cite{MP16} use QLSAs to implement the FEM for $d$-dimensional boundary value problems and evaluate the quantum speedup that can be achieved when estimating a function of the solution within precision $\epsilon$. This requires a careful analysis of how different algorithmic parameters (such as the dimension and condition number of the FEM linear system and the number of post-processing measurements) scale with respect to input variables (such as the spatial dimension $d$ and desired precision $\epsilon$), since all of these affect the complexity. Their algorithms have complexity $\poly(d, 1/\epsilon)$, compared to $O((1/\epsilon)^d)$ for the classical FEM. This exponential improvement with respect to $d$ suggests that quantum algorithms may be notably faster when $d$ is large. However, they also argue that for fixed $d$, at most a polynomial speed-up can be expected due to lower bounds on the cost of post-processing the state to estimate a function of the solution.

The FDM has also been used in quantum algorithms for PDEs. Reference~\cite{cao2013quantum} applies the FDM to solve Poisson's equation in rectangular volumes under Dirichlet boundary conditions. Reference~\cite{CJO17} applies the FDM to the problem of outputting states proportional to solutions of the wave equation, giving complexity $d^{\frac{5}{2}} \poly (1 / \epsilon )$, a polynomial dependence with $d$ and $1 / \epsilon $ (which is $\poly(n)$ for a fixed-order FDM). The FVM is combined with the reservoir method in Ref.~\cite{Fillion-Gourdeau2018} to simulate hyperbolic equations; although they achieve linear scaling with respect to the spatial dimension, they use fixed order differences, leading to $\poly (1/ \epsilon )$ scaling. These FDM, FEM, and FVM approaches can only give a total complexity $\poly(1/\epsilon)$, even using high-precision methods for the QLSA or Hamiltonian simulation, because the FDM, FEM, and FVM have additional approximation errors.

The FDM is also applied in Ref.~\cite{kivlichan2017bounding} to simulate how a fixed number of particles evolve under the Schr\"{o}dinger equation with access to an oracle for the potential term. This can be seen as a special case of quantum algorithms for PDEs. Other examples include quantum algorithms for many-body quantum dynamics \cite{zalka1998efficient,wiesner1996simulations} and for electronic structure problems, including for quantum chemistry (see for example \cite{Reiher7555, lanyon2010towards}). However, here we focus on PDEs whose dynamics are not necessarily unitary.

In this paper, we propose new quantum algorithms for linear PDEs. In the spirit of Ref.~\cite{MP16}, we state our results in terms of the approximation error and the spatial dimension; however, we do not consider the problem of estimating a function of the PDE solution and instead focus on outputting states encoding the solution, allowing us to give algorithms with complexity $\poly(\log(1/\epsilon))$. Just as for the QLSA, this improvement is potentially significant if the given equations must be solved as a subroutine within some larger computation. The problem we address can be informally stated as follows: Given a linear PDE with boundary conditions and an error parameter $\epsilon$, output a quantum state that is $\epsilon$-close to one whose amplitudes are proportional to the solution of the PDE at a set of grid points in the domain of the PDE.

Our first algorithm is based on a quantum version of the FDM approach: we use a finite-difference approximation to produce a system of linear equations and then solve that system using the QLSA. We analyze our FDM algorithm as applied to Poisson's equation under periodic, Dirichlet, and Neumann boundary conditions. Previous FDM approaches have considered fixed orders of truncation. Insteads we adjust the order of truncation depending on $\epsilon$, allowing more precise approximations. The main algorithm we present uses the quantum Fourier transform (QFT) and the LCU-based QLSA \cite{CKS15} and applies to periodic boundary conditions. However, by restricting to appropriate subspaces, this approach can also be applied to homogeneous Dirichlet and Neumann boundary conditions.

We also propose a quantum algorithm for more general second-order elliptic PDEs under periodic or non-periodic Dirichlet boundary conditions. This algorithm is based on quantum spectral methods \cite{CL19}. The spectral method globally approximates the solution of a PDE by a truncated Fourier or Chebyshev series (which converges exponentially for smooth functions) with undetermined coefficients, and then finds the coefficients by solving a linear system. This system is exponentially large in $d$, so solving it is infeasible for classical algorithms but feasible for quantum algorithms. To be able to apply the QLSA efficiently, we show how to make the system sparse using variants of the quantum Fourier transform.

Both of these approaches have complexity $\poly(d, \log(1/\epsilon))$, providing optimal dependence on $\epsilon$ and an exponential improvement over classical methods as a function of the spatial dimension $d$. Bounding the complexities of these algorithms requires analyzing how $d$ and $\epsilon$ affect the condition numbers of the relevant linear systems (finite difference matrices and matrices relating the spectral coefficients) and accounting for errors from the approximation methods the QLSA.

\tab{alg-compare} compares the performance of our approaches to other classical and quantum algorithms for PDEs. Compared to classical algorithms, quantum algorithms improve the dependence on spatial dimension from exponential to polynomial (with the caveat that they produce a different representation of the solution). Compared to previous quantum FDM/FEM/FVM algorithms \cite{cao2013quantum,Fillion-Gourdeau2018,CJO17,MP16}, the quantum adaptive FDM and quantum spectral method improve the error dependence from $\poly(1/\epsilon)$ to $\poly(\log(1/\epsilon))$. Our approaches achieve the best known dependence on parameters $d$ and $\epsilon$ for the Poisson equation with homogeneous boundary conditions. Furthermore, our quantum spectral method approach not only achieves the best known dependence on $d$ and $\epsilon$ for elliptic PDEs with inhomogeneous Dirichlet boundary conditions, but also improves the dependence on $d$ for the Poisson equation with inhomogeneous Dirichlet boundary conditions, as compared to previous quantum algorithms.

\begin{table}
\begin{center}
\footnotesize{
\renewcommand{\arraystretch}{1.25}
\begin{tabular}{|@{\hspace{1.5mm}}c|c|c|c|c|}
    \hline
     & \textbf{Algorithm} & \textbf{Equation} & \textbf{Boundary conditions}  & \textbf{Complexity} \\
    \hline
    \parbox[t]{1.5mm}{\multirow{5}{*}{\rotatebox[origin=c]{90}{Classical}}}
     & FDM/FEM/FVM  & general & general & $\poly((1/\epsilon)^d)$ \\
    \cline{2-5}
     & Adaptive FDM/FEM \cite{BG87}  & general & general & $\poly((\textcolor[rgb]{0.00,0.07,1.00}{\log(1/\epsilon)})^d)$ \\
    \cline{2-5}
     & Spectral method \cite{Ghe07,STW11} & general & general & $\poly((\textcolor[rgb]{0.00,0.07,1.00}{\log(1/\epsilon)})^d)$ \\
    \cline{2-5}
     & Sparse grid FDM/FEM \cite{BG04,Zen91}  & general & general & $\poly((1/\epsilon)(\log(1/\epsilon))^d)$ \\
    \cline{2-5}
     & Sparse grid spectral method \cite{SY10,SY12}  & elliptic & general & $\poly(\textcolor[rgb]{0.00,0.07,1.00}{\log(1/\epsilon)(\log\log(1/\epsilon))}^d)$ \\
    \hline
    \parbox[t]{1.5mm}{\multirow{7}{*}{\rotatebox[origin=c]{90}{Quantum}}}
     & FEM \cite{MP16}  & Poisson & homogeneous & $\poly(d, 1/\epsilon)$ \\
    \cline{2-5}
     & FDM \cite{cao2013quantum}  & Poisson & homogeneous Dirichlet & $\textcolor[rgb]{0.00,0.07,1.00}{d} \poly( \textcolor[rgb]{0.00,0.07,1.00}{\log d}, 1/\epsilon)$ \\
    \cline{2-5}
     & FDM \cite{CJO17}  & wave & homogeneous & $\textcolor[rgb]{0.00,0.07,1.00}{d^{5/2}} \poly(1/\epsilon)$ \\
    \cline{2-5}
     & FVM \cite{Fillion-Gourdeau2018}  & hyperbolic & periodic & $\textcolor[rgb]{0.00,0.07,1.00} d \poly(1/\epsilon)$ \\
    \cline{2-5}
     & Adaptive FDM [this paper]  & Poisson & periodic, homogeneous & $\textcolor[rgb]{0.00,0.07,1.00}{d^2} \poly( \textcolor[rgb]{0.00,0.07,1.00}{\log d}, \textcolor[rgb]{0.00,0.07,1.00}{\log(1/\epsilon)})$  \\
    \cline{2-5}
     & Spectral method [this paper]  & Poisson & homogeneous Dirichlet & $\textcolor[rgb]{0.00,0.07,1.00}{d} \poly(\textcolor[rgb]{0.00,0.07,1.00}{\log d}, \textcolor[rgb]{0.00,0.07,1.00}{\log(1/\epsilon)})$ \\
    \cline{2-5}
     & Spectral method [this paper]  & elliptic & inhomogeneous Dirichlet & $\textcolor[rgb]{0.00,0.07,1.00}{d^2} \poly(\textcolor[rgb]{0.00,0.07,1.00}{\log d}, \textcolor[rgb]{0.00,0.07,1.00}{\log(1/\epsilon)})$ \\
    \hline
\end{tabular}
}
\end{center}
\caption{
Summary of the time complexity of classical and quantum algorithms for $d$-dimensional PDEs within error tolerance $\epsilon$. Portions of the complexity in blue represent best known dependence on that parameter.
\label{tab:alg-compare}
}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\clearpage
\bibliographystyle{plain}
\bibliography{qspectral}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
